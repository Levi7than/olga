# Olga Language
Welcome to Olga, a simple programming language project designed for educational purposes. Olga is a compiled, statically-typed language influenced by the principles of Rust and C. This project serves as a valuable resource for those looking to delve into the world of compiler construction and gain a deeper understanding of language design.

## Overview
Olga is intentionally kept straightforward, making it an ideal platform for learning the essentials of building a compiler. The language draws inspiration from the robustness of Rust and the simplicity of C, providing users with a hands-on experience in constructing a compiler for a statically-typed language.

## Key Features
- **Compiled**: Olga is a compiled language, meaning that source code is translated into machine code for execution, enhancing performance and efficiency.
- **Static Typing**: Olga enforces strong static typing, catching potential errors at compile-time rather than runtime, contributing to code robustness.
- Influences from **Rust** and **C**: By combining features from Rust and C, Olga offers a familiar yet distinct programming experience.

## Project Details
Olga is developed in **C++23** standard with the usage of **ANTLR4** tool for parsing and **doctest** for testing.
Unfortunately, some tests like `test_global_repeated_occurrence` cannot be processed, as the project does not support exceptions.
Primarily some concepts were written in Rust and the project is to be re-written in this language. C++ is used only for university purposes.

## Getting Started
As there are some problems with antlr4-runtime for C++ (the project is written in C++23 standard, whereas the runtime uses C++17) one has to remember that _u8_ prefixes in must be manually removed from generated Lexer. In the future automatic script is to be written OR new runtime is to be used. Additionally, some warnings can be generated due to the divergence of C++ language standards.

Parser and Lexer is generated inside the src/parser directory with the following command:

`antlr4 -Dlanguage=Cpp -no-listener -visitor -o grammar olga.g4`

During the development process some functionalities may be unimplemented yet and thus the tests may create runtime errors. To disable them change:

`#define DOCTEST_CONFIG_IMPLEMENT` to `#define DOCTEST_CONFIG_DISABLE` in _main.cpp_

## License
Olga is open-source and released under the BSD 3-Clause License. Feel free to use, modify, and distribute the code in accordance with the terms specified in the license.

Copyright (C) 2023 Artur Twardzik
