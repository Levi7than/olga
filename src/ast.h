//
// Created by Artur Twardzik on 10/12/2023.
//

#ifndef OLGA_AST_H
#define OLGA_AST_H

#include <utility>
#include <vector>
#include <string>
#include <any>

#include "symbol_table.h"

/**
 * \brief Struct representing operation added as a node of the AST.
 */
enum NodeOperator {
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_BIT_AND,
    OP_BIT_XOR,
    OP_BIT_OR,
    OP_ASSIGN,
    OP_LOWER,
    OP_LOW_EQ,
    OP_GREATER,
    OP_GR_EQ,
    OP_EQUAL,
    OP_UNEQUAL,
    UNDEFINED,
};


/**
 * \brief Union representing exact value passed to an AST node.
 */
union exact_value {
    uint8_t u8;
    uint16_t u16;
    uint32_t u32;
    int8_t i8;
    int16_t i16;
    int32_t i32;
    const char *str;
};

/**
 * \brief Struct representing a node in AST.
 */
struct Node {
    Node() = delete;

    explicit Node(uint32_t destination_address) : r_operator(UNDEFINED), destination_address(destination_address) {}

    explicit Node(std::string_view identifier_reference)
            : r_operator(UNDEFINED), identifier_reference(identifier_reference) {}

    Node(NodeOperator r_operator, Node *left, Node *right) : r_operator(r_operator) {
        nodes.push_back(left);
        nodes.push_back(right);
    }

    Node(NodeOperator r_operator, uint32_t destination_address, Node *right)
            : r_operator(r_operator), destination_address(destination_address) {
        nodes.push_back(right);
    }

    Node(NodeOperator r_operator, uint32_t destination_address, uint32_t source_address)
            : r_operator(r_operator), destination_address(destination_address), source_address(source_address) {}

    Node(NodeOperator r_operator, uint32_t destination_address, exact_value e_val)
            : r_operator(r_operator), destination_address(destination_address), e_val(e_val) {}

    NodeOperator r_operator;

    std::vector<Node *> nodes;

    uint32_t destination_address{};

    uint32_t source_address{};

    std::string identifier_reference{};

    exact_value e_val;
};

/**
 * \brief Struct representing a whole function in AST.
 */
struct Fn {
    std::string identifier{};

    std::vector<Node *> body;

    std::vector<std::string> calls_within;

    ScopeSymbolTable *symbol_table;

    uint32_t address{};

    uint32_t address_offset = 0;

    Fn() = delete;

    explicit Fn(std::string_view identifier) : identifier(identifier) {
        symbol_table = new ScopeSymbolTable;
    }
};

#endif //OLGA_AST_H
