//
// Created by Artur Twardzik on 11/11/2023.
//

#include "cli.h"

#include "../tests/doctest.h"

#define OLGA_VERSION "0.0.1"

void print_help() {
    std::cout << "OLGA LANGUAGE COMPILER \nversion: " << OLGA_VERSION << std::endl << std::endl;

    std::cout << "USAGE: ./olga [flags]* <source file>" << std::endl;

    std::cout << "Flags: \n"
              << std::setw(20) << std::left << "-o <name>" << std::right << ": output executable file name\n"

              << std::setw(20) << std::left << "-v" << std::right << ": verbose mode\n"

              << std::setw(20) << std::left << "-t" << std::right
              << ": generate symbol table for the compilation unit\n"

              << std::setw(20) << std::left << "-a" << std::right
              << ": generate AST\n"

              << std::setw(20) << std::left << "-c" << std::right
              << ": check for recursive call chains\n"

              << std::setw(20) << std::left << "-r <scope>" << std::right
              << ": print allocations for a specific scope\n"

              << std::setw(20) << std::left << "-d" << std::right
              << ": print declarations for the whole compilation unit\n"

              << std::setw(20) << std::left << "-s" << std::right
              << ": print all scopes within the compilation unit\n"

              << std::setw(20) << std::left << "-g" << std::right
              << ": generate report consisting of all aforementioned options\n";
}

CLI::ProgramParameters parse_cli(const int argc, const char **argv) {
    CompilerFlags compiler_flags;
    CLI::ProgramParameters program_parameters;

    program_parameters.input_filename = argv[argc - 1];

    if (!contain_flags(argc, argv)) {
        return program_parameters;
    }

    std::vector<CLI::Flag> flags = read_cli_flags(argc, argv);


    for (const auto &flag: flags) {
        switch (flag.first) {
            case 'v':
                compiler_flags.verbose = true;
                break;
            case 'a':
                compiler_flags.get_AST = true;
                break;
            case 's':
                compiler_flags.get_scopes = true;
            case 't':
                compiler_flags.get_symbol_table = true;
                break;
            case 'd':
                compiler_flags.get_global_declarations = true;
                break;
            case 'g':
                compiler_flags.set_all();
                break;

            case 'c':
                compiler_flags.recursive_call_chain = true;
                break;
            case 'r':
                compiler_flags.get_scope_allocations = true;
                compiler_flags.scope = flag.second;
                break;
            case 'o':
                program_parameters.output_filename = flag.second;
                break;

            default:
                std::cerr << "[!] Unknown flag `-" << flag.first << "`. Skipping. \n";
                break;
        }
    }

    program_parameters.flags = compiler_flags;


    return program_parameters;
}

static bool contain_flags(const int argc, const char **argv) {
    for (int i = 0; i < argc - 1; ++i) {
        if (argv[i][0] == '-') {
            return true;
        }
    }
    return false;
}

static std::vector<CLI::Flag> read_cli_flags(const int argc, const char **argv) {
    std::vector<CLI::Flag> parameters;

    for (int i = argc - 2; i > 1; i--) {
        std::string_view parameter = argv[i];

        if (parameter.starts_with('-')) {
            parameters.emplace_back(parameter[1], "");
        }
        else {
            i--;
            std::string_view prefix = argv[i];
            parameters.emplace_back(prefix[1], parameter);
        }
    }

    std::reverse(parameters.begin(), parameters.end());

    return parameters;
}


TEST_SUITE_BEGIN("CLI");

TEST_CASE("parse_cli") {
    const int argc = 8;
    const char *argv[] = {"a.out", "-o", "output", "-a", "-v", "-r", "main", "source_code.og"};

    CLI::ProgramParameters expected_output = {
            "source_code.og",
            "output",
            CompilerFlags{
                    .verbose =  true,
                    .get_symbol_table =  false,
                    .get_AST = true,
                    .get_scopes = false,
                    .get_global_declarations = false,
                    .get_scope_allocations = true,
                    .scope = "main",
            }
    };

    CLI::ProgramParameters output = parse_cli(argc, argv);

    CHECK_EQ(output, expected_output);
}

TEST_CASE("contain_flags") {
    const int argc = 4;
    const char *argv[] = {"a.out", "-o", "output", "source_code.og"};

    CHECK(contain_flags(argc, argv));
}

TEST_CASE("contain_flags_without_flags") {
    const int argc = 2;
    const char *argv[] = {"a.out", "source_code.og"};

    CHECK_FALSE(contain_flags(argc, argv));
}

TEST_CASE("read_cli_flags") {
    const int argc = 7;
    const char *argv[] = {"a.out", "-o", "output", "-a", "-r", "main", "source_code.og"};

    std::vector<CLI::Flag> expected_output = {
            CLI::Flag(std::make_pair('o', "output")),
            CLI::Flag(std::make_pair('a', "")),
            CLI::Flag(std::make_pair('r', "main")),
    };

    std::vector<CLI::Flag> output = read_cli_flags(argc, argv);

    CHECK_EQ(output, expected_output);
}

TEST_CASE("read_cli_parameters_without_flags") {
    const int argc = 2;
    const char *argv[] = {"a.out", "source_code.og"};

    std::vector<CLI::Flag> expected_output = {};

    std::vector<CLI::Flag> output = read_cli_flags(argc, argv);

    CHECK_EQ(output, expected_output);
}

TEST_SUITE_END();
