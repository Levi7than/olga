//
// Created by Artur Twardzik on 11/11/2023.
//

#ifndef OLGA_CLI_H
#define OLGA_CLI_H

#include <string_view>
#include <vector>
#include <variant>

#include "compiler.h"


namespace CLI {
    struct ProgramParameters {
        std::string_view input_filename{};
        std::string_view output_filename{"a.out"};
        CompilerFlags flags;

        bool operator==(const ProgramParameters &other) const {
            if (input_filename == other.input_filename &&
                output_filename == other.output_filename &&
                flags == other.flags) {
                return true;
            }

            return false;
        }
    };

    using Flag = std::pair<char, std::string_view>;
}

/**
 * Prints the tutorial for CLI in Olga Language Compiler
 * */
void print_help();


/**
 * Parses command line arguments
 * @param argc number of parameters.
 * @param argv parameters given. They are represented as pure-C strings and passed as \b const.
 * Please note that the \b last parameter is input file name.
 * @return Struct holding input and output file (default set to `a.out`) as well as compiler flags
 * */
CLI::ProgramParameters parse_cli(int argc, const char **argv);

/**
 * Checks if the optional flags are supplied to the interface
 * */
static bool contain_flags(int argc, const char **argv);

/**
 * \b Unsafe for argc \< 3
 * Binds parameters with their values provided that the parameter takes an additional argument.
 * If not the parameter is passed with no value.
 * */
static std::vector<CLI::Flag> read_cli_flags(int argc, const char **argv);


#endif //OLGA_CLI_H
