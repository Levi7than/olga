//
// Created by Artur Twardzik on 11/11/2023.
//

#ifndef OLGA_COMPILER_H
#define OLGA_COMPILER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <string_view>
#include <map>
#include <vector>

#include "runtime/src/antlr4-runtime.h"
#include "parser/grammar/olgaLexer.h"
#include "parser/grammar/olgaParser.h"
#include "parser/grammar/olgaVisitor.h"
#include "symbol_table.h"
#include "ast.h"

#define WORD_SIZE 4

struct FilePrinter {
    std::ofstream ofstream;

    /**
     * Prints the stream into the report file
     * */
    void print(const std::stringstream &stream) {
        if (ofstream.good()) {
            ofstream << stream.str();
        }
    }

    FilePrinter(std::string_view filename) {
        ofstream.open(filename, std::ios::out);
    }

    ~FilePrinter() {
        ofstream.close();
    }
};


struct CompilerFlags {
    bool verbose{false};
    bool get_symbol_table{false};
    bool get_AST{false};
    bool get_scopes{false};
    bool get_global_declarations{false};
    bool recursive_call_chain{false};

    bool get_scope_allocations{false};
    std::string scope{};

    inline void set_all() {
        verbose = true;
        get_symbol_table = true;
        get_AST = true;
        get_scopes = true;
        get_global_declarations = true;
        recursive_call_chain = true;
    }

    friend std::ostream &operator<<(std::ostream &out, const CompilerFlags &r) {
        //please note that when std::boolalpha the std::setw does not work correctly
        out << "################# Compiler Flags #################\n"
                << std::boolalpha
                << std::left
                << std::setw(40) << "# Verbose: " << std::right << " -> " << r.verbose << std::left << "\n"
                << std::setw(40) << "# Get Symbol Table: " << std::right << " -> " << r.get_symbol_table << std::left
                << "\n"
                << std::setw(40) << "# Get AST: " << std::right << " -> " << r.get_AST << std::left << "\n"
                << std::setw(40) << "# Get Global Scopes: " << std::right << " -> " << r.get_scopes << std::left << "\n"
                << std::setw(40) << "# Get Global Declarations: " << std::right << " -> " << r.get_global_declarations
                << std::left << "\n"
                << std::setw(40) << "# /-Get Scope Allocations: " << std::right << " -> " << r.get_scope_allocations
                << std::left << "\n"
                << (r.get_scope_allocations ? "# \\-   |-> For scope: `" + r.scope + "`" : "") << "\n"
                << std::noboolalpha
                << std::setw(0)
                << "##################################################"
                << std::endl;

        return out;
    }

    bool operator==(const CompilerFlags &other) const = default;
};


class ParseTreeVisitor : public olgaVisitor {
public:
    antlrcpp::Any visitCompilationUnit(olgaParser::CompilationUnitContext *context) override {
        return antlr4::tree::AbstractParseTreeVisitor::visitChildren(context);
    }

    /**
     * External declaration is the first stage after compilation unit that can hold functions and variables declarations.
     * */
    antlrcpp::Any visitExternalDeclaration(olgaParser::ExternalDeclarationContext *context) override {
        //        std::string nxt = context->functionDefinition()->sizeSpecifier()->getText();
        //        std::cout << "Provided function returns: " << nxt << std::endl;

        //        context->functionDefinition()->accept(this);
        antlr4::tree::AbstractParseTreeVisitor::visitChildren(context);

        //        std::cout << symbol_table.get_global_printable() << std::endl;
        return 0;
    }

    antlrcpp::Any visitCompoundStatement(olgaParser::CompoundStatementContext *context) override {
        //        context->declaration()->accept(this);
        antlr4::tree::AbstractParseTreeVisitor::visitChildren(context);

        return 0;
    }

    antlrcpp::Any visitJumpStatement(olgaParser::JumpStatementContext *context) override {
        return 0;
    }

    antlrcpp::Any visitIfStatement(olgaParser::IfStatementContext *context) override {
    }

    antlrcpp::Any visitWhileStatement(olgaParser::WhileStatementContext *context) override {
    }

    /**
     * \brief Visits function call and parses it into AST.
     */
    antlrcpp::Any visitFunctionCall(olgaParser::FunctionCallContext *context) override {
        Node *fn_call = new Node(context->Identifier()->getText());

        if (global_scope_traversal) {
            ast_nodes.push_back(fn_call);
        }
        else {
            current_function->body.push_back(fn_call);
            current_function->calls_within.push_back(
                fn_call->identifier_reference); //temporary solution for university purpose
        }

        return 0;
    }

    antlrcpp::Any visitExpression(olgaParser::ExpressionContext *context) override {
        //for now visit only expression->functionCall
        antlr4::tree::AbstractParseTreeVisitor::visitChildren(context);

        return 0;
    }

    antlrcpp::Any visitAssignmentExpression(olgaParser::AssignmentExpressionContext *context) override {
    }

    /**
     * \brief Visits declaration and parses it into AST and symbol table.
     */
    antlrcpp::Any visitDeclaration(olgaParser::DeclarationContext *context) override {
        std::string identifier = context->variableDeclaration()->Identifier()->getText();
        DataSize data_size = determine_data_size(context->variableDeclaration()->sizeSpecifier()->getText());

        SymbolEntry symbol_entry{
            .identifier = identifier,
            .data_size = data_size,
            .symbol_type = SymbolType::Variable,
            .line_number = context->getStart()->getLine(),
        };

        Node *node = new Node(identifier);
        if (global_scope_traversal) {
            symbol_table.insert_globally(symbol_entry);

            ast_nodes.push_back(node);
        }
        else {
            current_function->symbol_table->insert(symbol_entry);

            current_function->address_offset += get_data_width(data_size);
            current_function->body.push_back(node);
        }

        return 0;
    }

    antlrcpp::Any visitDefinition(olgaParser::DefinitionContext *context) override {
        context->variableDefinition()->accept(this);

        return 0;
    }

    antlrcpp::Any visitSizeSpecifier(olgaParser::SizeSpecifierContext *context) override {
        return 0;
    }

    antlrcpp::Any visitPointerDeclaration(olgaParser::PointerDeclarationContext *context) override {
    }

    antlrcpp::Any visitVariableDeclaration(olgaParser::VariableDeclarationContext *context) override {
    }

    antlrcpp::Any visitArrayDeclaration(olgaParser::ArrayDeclarationContext *context) override {
    }

    antlrcpp::Any visitPointerDefinition(olgaParser::PointerDefinitionContext *context) override {
    }

    /**
     * \brief Visits variable definition and parses it into AST and symbol table.
     */
    antlrcpp::Any visitVariableDefinition(olgaParser::VariableDefinitionContext *context) override {
        std::string identifier = context->variableDeclaration()->Identifier()->getText();
        DataSize data_size = determine_data_size(context->variableDeclaration()->sizeSpecifier()->getText());

        SymbolEntry symbol_entry{
            .identifier = identifier,
            .data_size = data_size,
            .symbol_type = SymbolType::Variable,
            .line_number = context->getStart()->getLine(),
        };

        Node *node = new Node(identifier);
        if (global_scope_traversal) {
            symbol_table.insert_globally(symbol_entry);

            ast_nodes.push_back(node);
        }
        else {
            current_function->symbol_table->insert(symbol_entry);

            current_function->address_offset += get_data_width(data_size);
            current_function->body.push_back(node);
        }
        ////////////////////////////////////

        context->expression()->accept(this);

        if (global_scope_traversal) {
            Node *expression = ast_nodes.back();
            ast_nodes.pop_back();
            Node *declaration = ast_nodes.back();
            ast_nodes.pop_back();

            Node *definition = new Node(OP_ASSIGN, declaration, expression);
            ast_nodes.push_back(definition);
        }
        else {
            Node *expression = current_function->body.back();
            current_function->body.pop_back();

            Node *declaration = current_function->body.back();
            current_function->body.pop_back();

            Node *definition = new Node(OP_ASSIGN, declaration, expression);
            current_function->body.push_back(definition);
        }

        return 0;
    }

    antlrcpp::Any visitArrayDefinition(olgaParser::ArrayDefinitionContext *context) override {
    }

    antlrcpp::Any visitInitializer(olgaParser::InitializerContext *context) override {
    }

    /**
     * \brief Visits function definition and parses it into AST and symbol table.
     */
    antlrcpp::Any visitFunctionDefinition(olgaParser::FunctionDefinitionContext *context) override {
        global_scope_traversal = false;
        std::string identifier = context->Identifier()->getText();
        DataSize data_size = determine_data_size(context->sizeSpecifier()->getText());

        SymbolEntry symbol_entry{
            .identifier = identifier,
            .data_size = data_size,
            .symbol_type = SymbolType::Function,
            .line_number = context->getStart()->getLine(),
        };

        symbol_table.insert_globally(symbol_entry);

        Fn *f = new Fn(identifier);
        current_function = f;
        current_function->symbol_table->scope_name = identifier;

        antlr4::tree::AbstractParseTreeVisitor::visitChildren(context);

        for (const auto &symbol: current_function->symbol_table->scope_symbols) {
            symbol_table.insert_in_scope(symbol, current_function->symbol_table->scope_name);
        }

        functions.push_back(current_function);
        global_scope_traversal = true;
        return 0;
    }

    antlrcpp::Any visitParameterDeclarationList(olgaParser::ParameterDeclarationListContext *context) override {
        return 0;
    }

    antlrcpp::Any visitParameterDeclaration(olgaParser::ParameterDeclarationContext *context) override {
    }

    antlrcpp::Any visitPrefixOperator(olgaParser::PrefixOperatorContext *context) override {
    }

    antlrcpp::Any visitDereferenceOperator(olgaParser::DereferenceOperatorContext *context) override {
    }

    antlrcpp::Any visitReferenceOperator(olgaParser::ReferenceOperatorContext *context) override {
    }

    antlrcpp::Any visitNegationOperator(olgaParser::NegationOperatorContext *context) override {
    }

    antlrcpp::Any visitAssignmentOperator(olgaParser::AssignmentOperatorContext *context) override {
    }

    antlrcpp::Any visitR_operator(olgaParser::R_operatorContext *context) override {
    }

    antlrcpp::Any visitArithmeticalOperator(olgaParser::ArithmeticalOperatorContext *context) override {
    }

    antlrcpp::Any visitLogicalOperator(olgaParser::LogicalOperatorContext *context) override {
    }

    antlrcpp::Any visitRelationalOperator(olgaParser::RelationalOperatorContext *context) override {
    }

    antlrcpp::Any visitShiftOperator(olgaParser::ShiftOperatorContext *context) override {
    }

    antlrcpp::Any visitBitOperator(olgaParser::BitOperatorContext *context) override {
    }

private:
    bool verbose{false};

    bool global_scope_traversal{true};

    GlobalSymbolTable symbol_table;

    std::vector<Node *> ast_nodes;

    std::vector<Fn *> functions;

    Fn *current_function;

    std::string fn_identifier{};

public:
    /**
     * \return Returns Global Symbol Table in a printable format.
     */
    std::stringstream get_symbol_table_printable() {
        std::stringstream ss;
        ss << symbol_table.get_global_printable() << std::endl;

        return ss;
    };

    /**
     * \return Returns Global Symbol Table
     */
    const GlobalSymbolTable &get_symbol_table() {
        return symbol_table;
    }

    /**
     * Returns identifiers of all scopes declared globally.
     * */
    std::vector<std::string> get_global_scopes() {
        std::vector<std::string> global_symbols;
        for (const auto &symbol: symbol_table.global_symbols) {
            if (symbol.symbol_type == SymbolType::Function) {
                global_symbols.push_back(symbol.identifier);
            }
        }
    }

    std::stringstream get_global_declarations() {
        return std::stringstream("Global Declarations not implemented yet.");
    }

    std::stringstream get_allocations_for_scope() {
        return std::stringstream("Scope allocations not implemented yet.");
    }

    std::stringstream get_ast() {
        return std::stringstream("AST not implemented yet.");
    }

    /**
     * \return Vector of AST functions.
     */
    const std::vector<Fn *> get_functions() {
        return functions;
    }
};

#endif //OLGA_COMPILER_H
