#include "cli.h"
#include "compiler.h"
#include "static_analyzer.h"

#define DOCTEST_CONFIG_IMPLEMENT

#include "../tests/doctest.h"

/*
 * Grammar is generated with the following command:
 *  antlr4 -Dlanguage=Cpp -no-listener -visitor -o grammar olga.g4
 *
 *  While providing the parameters to the program, the source code input file is the last one given
 *
 *  For the purposes of the first stages of the project output file will not be the executable and will hold generated
 *  reports.
 *
 *  Please bear in mind that one has to manually remove 'u8' prefixes after lexer generation, as the new compiler does
 *  not support constructor for std::vector<std::string> with utf-8 strings. FIXME: automatic script should be added
 *
 *  Also the C++2x compiler may generate warnings, as the antlr4 runtime is written entirely in C++17 standard
 *
 *
 *  Additional, worth to mention sources:
 *  Compilers tiny projects:
 *  - https://forge.univ-lyon1.fr/ERIC.GUERIN/antlr Tiny C - only main
 *  - https://github.com/jeromehue/PLD-COMP/tree/main C compiler written in C++
 *  - https://github.com/parrt/cs652/blob/master/lectures/code/symtab/src/DefSymbolsAndScopes.java
 *  - https://stackoverflow.com/questions/20714492/antlr4-listeners-and-visitors-which-to-implement
 *
 * Filling the Symbol Table:
 *  - https://stackoverflow.com/questions/16363348/compiler-what-is-the-best-way-to-fill-the-symbol-table?rq=4
 *
 * Generating call grap:
 *  - https://medium.com/pragmatic-programmers/generating-a-call-graph-1b267b400c33
 *
 *  Testing framework:
 *  - https://github.com/doctest/doctest
 *
 *  Why hating goto statement is fundamentally bad:
 *  - https://koblents.com/Ches/Links/Month-Mar-2013/20-Using-Goto-in-Linux-Kernel-Code/
 */


int main(int argc, char **argv) {
    //in order to force clion to respect test configurations one has to manually exclude -r=xml parameter for tests only
    if (strcmp(argv[1], "-r=xml") == 0) {
        doctest::Context context;

        context.setOption("abort-after", 5);
        context.setOption("order-by", "name");

        context.applyCommandLine(argc, argv);

        context.setOption("no-breaks", true);

        int res = context.run();

        if (context.shouldExit()) {
            return res;
        }

        int client_stuff_return_code = 0;

        return res + client_stuff_return_code;
    }

    // --------------------------

    if (argc == 1) {
        std::cerr << "[!] No input file given. Aborting.\n";
        return 1;
    }
    else if (strcmp(argv[1], "--help") == 0) {
        print_help();
        return 0;
    }

    CLI::ProgramParameters params = parse_cli(argc, (const char **) argv);
    std::cout << "Input: " << params.input_filename << "\n"
              << "Output: " << params.output_filename << "\n";
//              << params.flags;

    std::ifstream stream;
    stream.open(params.input_filename);

    antlr4::ANTLRInputStream input(stream);
    olgaLexer lexer(&input);
    antlr4::CommonTokenStream tokens(&lexer);

    olgaParser parser(&tokens);

    antlr4::tree::ParseTree *tree = parser.compilationUnit();

    std::cout << tree->toStringTree(&parser, true) << std::endl;

    ParseTreeVisitor visitor;
    visitor.visit(tree);
    FilePrinter printer("REPORT");

    printer.print(visitor.get_symbol_table_printable());

    //////////////

    auto call_map = create_calls_map(visitor.get_functions(), visitor.get_symbol_table());

    print_calls(call_map);

    auto call_graph = create_call_graph(call_map);


    std::cout << "\n--------------------\n";

    for (const auto *call: call_graph) {
        if (!call->contains_calls) {
            continue;
        }

        size_t depth = 0;
        std::string inner_calls = get_following_calls(call, call, depth, call_graph);
        if (has_recursive_call_chain(inner_calls)) {
            std::cout << inner_calls << std::endl;
        }
    }

    return 0;
}

