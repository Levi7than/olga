
// Generated from olga.g4 by ANTLR 4.8


#include "olgaVisitor.h"

#include "olgaParser.h"


using namespace antlrcpp;
using namespace antlr4;

olgaParser::olgaParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

olgaParser::~olgaParser() {
  delete _interpreter;
}

std::string olgaParser::getGrammarFileName() const {
  return "olga.g4";
}

const std::vector<std::string>& olgaParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& olgaParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- CompilationUnitContext ------------------------------------------------------------------

olgaParser::CompilationUnitContext::CompilationUnitContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<olgaParser::ExternalDeclarationContext *> olgaParser::CompilationUnitContext::externalDeclaration() {
  return getRuleContexts<olgaParser::ExternalDeclarationContext>();
}

olgaParser::ExternalDeclarationContext* olgaParser::CompilationUnitContext::externalDeclaration(size_t i) {
  return getRuleContext<olgaParser::ExternalDeclarationContext>(i);
}


size_t olgaParser::CompilationUnitContext::getRuleIndex() const {
  return olgaParser::RuleCompilationUnit;
}


antlrcpp::Any olgaParser::CompilationUnitContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitCompilationUnit(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::CompilationUnitContext* olgaParser::compilationUnit() {
  CompilationUnitContext *_localctx = _tracker.createInstance<CompilationUnitContext>(_ctx, getState());
  enterRule(_localctx, 0, olgaParser::RuleCompilationUnit);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(67); 
    _errHandler->sync(this);
    _la = _input->LA(1);
    do {
      setState(66);
      externalDeclaration();
      setState(69); 
      _errHandler->sync(this);
      _la = _input->LA(1);
    } while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::Let)
      | (1ULL << olgaParser::Ptr)
      | (1ULL << olgaParser::Fn))) != 0));
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExternalDeclarationContext ------------------------------------------------------------------

olgaParser::ExternalDeclarationContext::ExternalDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::FunctionDefinitionContext* olgaParser::ExternalDeclarationContext::functionDefinition() {
  return getRuleContext<olgaParser::FunctionDefinitionContext>(0);
}

olgaParser::DeclarationContext* olgaParser::ExternalDeclarationContext::declaration() {
  return getRuleContext<olgaParser::DeclarationContext>(0);
}

olgaParser::DefinitionContext* olgaParser::ExternalDeclarationContext::definition() {
  return getRuleContext<olgaParser::DefinitionContext>(0);
}


size_t olgaParser::ExternalDeclarationContext::getRuleIndex() const {
  return olgaParser::RuleExternalDeclaration;
}


antlrcpp::Any olgaParser::ExternalDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitExternalDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ExternalDeclarationContext* olgaParser::externalDeclaration() {
  ExternalDeclarationContext *_localctx = _tracker.createInstance<ExternalDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 2, olgaParser::RuleExternalDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(78);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(71);
      functionDefinition();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(72);
      declaration();
      setState(73);
      match(olgaParser::T__0);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(75);
      definition();
      setState(76);
      match(olgaParser::T__0);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- CompoundStatementContext ------------------------------------------------------------------

olgaParser::CompoundStatementContext::CompoundStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::JumpStatementContext* olgaParser::CompoundStatementContext::jumpStatement() {
  return getRuleContext<olgaParser::JumpStatementContext>(0);
}

olgaParser::DeclarationContext* olgaParser::CompoundStatementContext::declaration() {
  return getRuleContext<olgaParser::DeclarationContext>(0);
}

olgaParser::DefinitionContext* olgaParser::CompoundStatementContext::definition() {
  return getRuleContext<olgaParser::DefinitionContext>(0);
}

olgaParser::AssignmentExpressionContext* olgaParser::CompoundStatementContext::assignmentExpression() {
  return getRuleContext<olgaParser::AssignmentExpressionContext>(0);
}

olgaParser::IfStatementContext* olgaParser::CompoundStatementContext::ifStatement() {
  return getRuleContext<olgaParser::IfStatementContext>(0);
}

olgaParser::WhileStatementContext* olgaParser::CompoundStatementContext::whileStatement() {
  return getRuleContext<olgaParser::WhileStatementContext>(0);
}

olgaParser::FunctionCallContext* olgaParser::CompoundStatementContext::functionCall() {
  return getRuleContext<olgaParser::FunctionCallContext>(0);
}


size_t olgaParser::CompoundStatementContext::getRuleIndex() const {
  return olgaParser::RuleCompoundStatement;
}


antlrcpp::Any olgaParser::CompoundStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitCompoundStatement(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::CompoundStatementContext* olgaParser::compoundStatement() {
  CompoundStatementContext *_localctx = _tracker.createInstance<CompoundStatementContext>(_ctx, getState());
  enterRule(_localctx, 4, olgaParser::RuleCompoundStatement);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(95);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 2, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(80);
      jumpStatement();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(81);
      declaration();
      setState(82);
      match(olgaParser::T__0);
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(84);
      definition();
      setState(85);
      match(olgaParser::T__0);
      break;
    }

    case 4: {
      enterOuterAlt(_localctx, 4);
      setState(87);
      assignmentExpression();
      setState(88);
      match(olgaParser::T__0);
      break;
    }

    case 5: {
      enterOuterAlt(_localctx, 5);
      setState(90);
      ifStatement();
      break;
    }

    case 6: {
      enterOuterAlt(_localctx, 6);
      setState(91);
      whileStatement();
      break;
    }

    case 7: {
      enterOuterAlt(_localctx, 7);
      setState(92);
      functionCall();
      setState(93);
      match(olgaParser::T__0);
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- JumpStatementContext ------------------------------------------------------------------

olgaParser::JumpStatementContext::JumpStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::JumpStatementContext::Return() {
  return getToken(olgaParser::Return, 0);
}

tree::TerminalNode* olgaParser::JumpStatementContext::Literal() {
  return getToken(olgaParser::Literal, 0);
}

tree::TerminalNode* olgaParser::JumpStatementContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

tree::TerminalNode* olgaParser::JumpStatementContext::Break() {
  return getToken(olgaParser::Break, 0);
}

tree::TerminalNode* olgaParser::JumpStatementContext::Continue() {
  return getToken(olgaParser::Continue, 0);
}


size_t olgaParser::JumpStatementContext::getRuleIndex() const {
  return olgaParser::RuleJumpStatement;
}


antlrcpp::Any olgaParser::JumpStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitJumpStatement(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::JumpStatementContext* olgaParser::jumpStatement() {
  JumpStatementContext *_localctx = _tracker.createInstance<JumpStatementContext>(_ctx, getState());
  enterRule(_localctx, 6, olgaParser::RuleJumpStatement);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(104);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case olgaParser::Return: {
        enterOuterAlt(_localctx, 1);
        setState(97);
        match(olgaParser::Return);
        setState(98);
        _la = _input->LA(1);
        if (!(_la == olgaParser::Identifier

        || _la == olgaParser::Literal)) {
        _errHandler->recoverInline(this);
        }
        else {
          _errHandler->reportMatch(this);
          consume();
        }
        setState(99);
        match(olgaParser::T__0);
        break;
      }

      case olgaParser::Break: {
        enterOuterAlt(_localctx, 2);
        setState(100);
        match(olgaParser::Break);
        setState(101);
        match(olgaParser::T__0);
        break;
      }

      case olgaParser::Continue: {
        enterOuterAlt(_localctx, 3);
        setState(102);
        match(olgaParser::Continue);
        setState(103);
        match(olgaParser::T__0);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- IfStatementContext ------------------------------------------------------------------

olgaParser::IfStatementContext::IfStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::IfStatementContext::If() {
  return getToken(olgaParser::If, 0);
}

olgaParser::ExpressionContext* olgaParser::IfStatementContext::expression() {
  return getRuleContext<olgaParser::ExpressionContext>(0);
}

std::vector<olgaParser::CompoundStatementContext *> olgaParser::IfStatementContext::compoundStatement() {
  return getRuleContexts<olgaParser::CompoundStatementContext>();
}

olgaParser::CompoundStatementContext* olgaParser::IfStatementContext::compoundStatement(size_t i) {
  return getRuleContext<olgaParser::CompoundStatementContext>(i);
}


size_t olgaParser::IfStatementContext::getRuleIndex() const {
  return olgaParser::RuleIfStatement;
}


antlrcpp::Any olgaParser::IfStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitIfStatement(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::IfStatementContext* olgaParser::ifStatement() {
  IfStatementContext *_localctx = _tracker.createInstance<IfStatementContext>(_ctx, getState());
  enterRule(_localctx, 8, olgaParser::RuleIfStatement);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(106);
    match(olgaParser::If);
    setState(107);
    match(olgaParser::T__1);
    setState(108);
    expression();
    setState(109);
    match(olgaParser::T__2);
    setState(110);
    match(olgaParser::T__3);
    setState(114);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::If)
      | (1ULL << olgaParser::While)
      | (1ULL << olgaParser::Let)
      | (1ULL << olgaParser::Ptr)
      | (1ULL << olgaParser::Break)
      | (1ULL << olgaParser::Continue)
      | (1ULL << olgaParser::Return)
      | (1ULL << olgaParser::Identifier))) != 0)) {
      setState(111);
      compoundStatement();
      setState(116);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(117);
    match(olgaParser::T__4);
    setState(127);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == olgaParser::T__5) {
      setState(118);
      match(olgaParser::T__5);
      setState(119);
      match(olgaParser::T__3);
      setState(123);
      _errHandler->sync(this);
      _la = _input->LA(1);
      while ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << olgaParser::T__12)
        | (1ULL << olgaParser::T__13)
        | (1ULL << olgaParser::T__14)
        | (1ULL << olgaParser::If)
        | (1ULL << olgaParser::While)
        | (1ULL << olgaParser::Let)
        | (1ULL << olgaParser::Ptr)
        | (1ULL << olgaParser::Break)
        | (1ULL << olgaParser::Continue)
        | (1ULL << olgaParser::Return)
        | (1ULL << olgaParser::Identifier))) != 0)) {
        setState(120);
        compoundStatement();
        setState(125);
        _errHandler->sync(this);
        _la = _input->LA(1);
      }
      setState(126);
      match(olgaParser::T__4);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- WhileStatementContext ------------------------------------------------------------------

olgaParser::WhileStatementContext::WhileStatementContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::WhileStatementContext::While() {
  return getToken(olgaParser::While, 0);
}

olgaParser::ExpressionContext* olgaParser::WhileStatementContext::expression() {
  return getRuleContext<olgaParser::ExpressionContext>(0);
}

std::vector<olgaParser::CompoundStatementContext *> olgaParser::WhileStatementContext::compoundStatement() {
  return getRuleContexts<olgaParser::CompoundStatementContext>();
}

olgaParser::CompoundStatementContext* olgaParser::WhileStatementContext::compoundStatement(size_t i) {
  return getRuleContext<olgaParser::CompoundStatementContext>(i);
}


size_t olgaParser::WhileStatementContext::getRuleIndex() const {
  return olgaParser::RuleWhileStatement;
}


antlrcpp::Any olgaParser::WhileStatementContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitWhileStatement(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::WhileStatementContext* olgaParser::whileStatement() {
  WhileStatementContext *_localctx = _tracker.createInstance<WhileStatementContext>(_ctx, getState());
  enterRule(_localctx, 10, olgaParser::RuleWhileStatement);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(129);
    match(olgaParser::While);
    setState(130);
    match(olgaParser::T__1);
    setState(131);
    expression();
    setState(132);
    match(olgaParser::T__2);
    setState(133);
    match(olgaParser::T__3);
    setState(137);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::If)
      | (1ULL << olgaParser::While)
      | (1ULL << olgaParser::Let)
      | (1ULL << olgaParser::Ptr)
      | (1ULL << olgaParser::Break)
      | (1ULL << olgaParser::Continue)
      | (1ULL << olgaParser::Return)
      | (1ULL << olgaParser::Identifier))) != 0)) {
      setState(134);
      compoundStatement();
      setState(139);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(140);
    match(olgaParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionCallContext ------------------------------------------------------------------

olgaParser::FunctionCallContext::FunctionCallContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::FunctionCallContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

std::vector<olgaParser::ExpressionContext *> olgaParser::FunctionCallContext::expression() {
  return getRuleContexts<olgaParser::ExpressionContext>();
}

olgaParser::ExpressionContext* olgaParser::FunctionCallContext::expression(size_t i) {
  return getRuleContext<olgaParser::ExpressionContext>(i);
}


size_t olgaParser::FunctionCallContext::getRuleIndex() const {
  return olgaParser::RuleFunctionCall;
}


antlrcpp::Any olgaParser::FunctionCallContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitFunctionCall(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::FunctionCallContext* olgaParser::functionCall() {
  FunctionCallContext *_localctx = _tracker.createInstance<FunctionCallContext>(_ctx, getState());
  enterRule(_localctx, 12, olgaParser::RuleFunctionCall);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(142);
    match(olgaParser::Identifier);
    setState(143);
    match(olgaParser::T__1);
    setState(145);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__8)
      | (1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::Identifier)
      | (1ULL << olgaParser::Literal))) != 0)) {
      setState(144);
      expression();
    }
    setState(151);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == olgaParser::T__6) {
      setState(147);
      match(olgaParser::T__6);
      setState(148);
      expression();
      setState(153);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(154);
    match(olgaParser::T__2);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExpressionContext ------------------------------------------------------------------

olgaParser::ExpressionContext::ExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<tree::TerminalNode *> olgaParser::ExpressionContext::Identifier() {
  return getTokens(olgaParser::Identifier);
}

tree::TerminalNode* olgaParser::ExpressionContext::Identifier(size_t i) {
  return getToken(olgaParser::Identifier, i);
}

std::vector<tree::TerminalNode *> olgaParser::ExpressionContext::Literal() {
  return getTokens(olgaParser::Literal);
}

tree::TerminalNode* olgaParser::ExpressionContext::Literal(size_t i) {
  return getToken(olgaParser::Literal, i);
}

std::vector<olgaParser::FunctionCallContext *> olgaParser::ExpressionContext::functionCall() {
  return getRuleContexts<olgaParser::FunctionCallContext>();
}

olgaParser::FunctionCallContext* olgaParser::ExpressionContext::functionCall(size_t i) {
  return getRuleContext<olgaParser::FunctionCallContext>(i);
}

std::vector<olgaParser::InitializerContext *> olgaParser::ExpressionContext::initializer() {
  return getRuleContexts<olgaParser::InitializerContext>();
}

olgaParser::InitializerContext* olgaParser::ExpressionContext::initializer(size_t i) {
  return getRuleContext<olgaParser::InitializerContext>(i);
}

std::vector<olgaParser::PrefixOperatorContext *> olgaParser::ExpressionContext::prefixOperator() {
  return getRuleContexts<olgaParser::PrefixOperatorContext>();
}

olgaParser::PrefixOperatorContext* olgaParser::ExpressionContext::prefixOperator(size_t i) {
  return getRuleContext<olgaParser::PrefixOperatorContext>(i);
}

olgaParser::R_operatorContext* olgaParser::ExpressionContext::r_operator() {
  return getRuleContext<olgaParser::R_operatorContext>(0);
}


size_t olgaParser::ExpressionContext::getRuleIndex() const {
  return olgaParser::RuleExpression;
}


antlrcpp::Any olgaParser::ExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitExpression(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ExpressionContext* olgaParser::expression() {
  ExpressionContext *_localctx = _tracker.createInstance<ExpressionContext>(_ctx, getState());
  enterRule(_localctx, 14, olgaParser::RuleExpression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(157);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14))) != 0)) {
      setState(156);
      prefixOperator();
    }
    setState(163);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 11, _ctx)) {
    case 1: {
      setState(159);
      match(olgaParser::Identifier);
      break;
    }

    case 2: {
      setState(160);
      match(olgaParser::Literal);
      break;
    }

    case 3: {
      setState(161);
      functionCall();
      break;
    }

    case 4: {
      setState(162);
      initializer();
      break;
    }

    }
    setState(175);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__8)
      | (1ULL << olgaParser::T__9)
      | (1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::T__19)
      | (1ULL << olgaParser::T__20)
      | (1ULL << olgaParser::T__21)
      | (1ULL << olgaParser::T__22)
      | (1ULL << olgaParser::T__23)
      | (1ULL << olgaParser::T__24)
      | (1ULL << olgaParser::T__25)
      | (1ULL << olgaParser::T__26)
      | (1ULL << olgaParser::T__27)
      | (1ULL << olgaParser::T__28)
      | (1ULL << olgaParser::T__29)
      | (1ULL << olgaParser::T__30)
      | (1ULL << olgaParser::T__31)
      | (1ULL << olgaParser::T__32))) != 0)) {
      setState(165);
      r_operator();
      setState(167);
      _errHandler->sync(this);

      _la = _input->LA(1);
      if ((((_la & ~ 0x3fULL) == 0) &&
        ((1ULL << _la) & ((1ULL << olgaParser::T__12)
        | (1ULL << olgaParser::T__13)
        | (1ULL << olgaParser::T__14))) != 0)) {
        setState(166);
        prefixOperator();
      }
      setState(173);
      _errHandler->sync(this);
      switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 13, _ctx)) {
      case 1: {
        setState(169);
        match(olgaParser::Identifier);
        break;
      }

      case 2: {
        setState(170);
        match(olgaParser::Literal);
        break;
      }

      case 3: {
        setState(171);
        functionCall();
        break;
      }

      case 4: {
        setState(172);
        initializer();
        break;
      }

      }
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentExpressionContext ------------------------------------------------------------------

olgaParser::AssignmentExpressionContext::AssignmentExpressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::AssignmentExpressionContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::AssignmentOperatorContext* olgaParser::AssignmentExpressionContext::assignmentOperator() {
  return getRuleContext<olgaParser::AssignmentOperatorContext>(0);
}

olgaParser::ExpressionContext* olgaParser::AssignmentExpressionContext::expression() {
  return getRuleContext<olgaParser::ExpressionContext>(0);
}

olgaParser::PrefixOperatorContext* olgaParser::AssignmentExpressionContext::prefixOperator() {
  return getRuleContext<olgaParser::PrefixOperatorContext>(0);
}


size_t olgaParser::AssignmentExpressionContext::getRuleIndex() const {
  return olgaParser::RuleAssignmentExpression;
}


antlrcpp::Any olgaParser::AssignmentExpressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitAssignmentExpression(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::AssignmentExpressionContext* olgaParser::assignmentExpression() {
  AssignmentExpressionContext *_localctx = _tracker.createInstance<AssignmentExpressionContext>(_ctx, getState());
  enterRule(_localctx, 16, olgaParser::RuleAssignmentExpression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(178);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14))) != 0)) {
      setState(177);
      prefixOperator();
    }
    setState(180);
    match(olgaParser::Identifier);
    setState(181);
    assignmentOperator();
    setState(182);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DeclarationContext ------------------------------------------------------------------

olgaParser::DeclarationContext::DeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::PointerDeclarationContext* olgaParser::DeclarationContext::pointerDeclaration() {
  return getRuleContext<olgaParser::PointerDeclarationContext>(0);
}

olgaParser::VariableDeclarationContext* olgaParser::DeclarationContext::variableDeclaration() {
  return getRuleContext<olgaParser::VariableDeclarationContext>(0);
}

olgaParser::ArrayDeclarationContext* olgaParser::DeclarationContext::arrayDeclaration() {
  return getRuleContext<olgaParser::ArrayDeclarationContext>(0);
}


size_t olgaParser::DeclarationContext::getRuleIndex() const {
  return olgaParser::RuleDeclaration;
}


antlrcpp::Any olgaParser::DeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::DeclarationContext* olgaParser::declaration() {
  DeclarationContext *_localctx = _tracker.createInstance<DeclarationContext>(_ctx, getState());
  enterRule(_localctx, 18, olgaParser::RuleDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(187);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 16, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(184);
      pointerDeclaration();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(185);
      variableDeclaration();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(186);
      arrayDeclaration();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DefinitionContext ------------------------------------------------------------------

olgaParser::DefinitionContext::DefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::PointerDefinitionContext* olgaParser::DefinitionContext::pointerDefinition() {
  return getRuleContext<olgaParser::PointerDefinitionContext>(0);
}

olgaParser::VariableDefinitionContext* olgaParser::DefinitionContext::variableDefinition() {
  return getRuleContext<olgaParser::VariableDefinitionContext>(0);
}

olgaParser::ArrayDefinitionContext* olgaParser::DefinitionContext::arrayDefinition() {
  return getRuleContext<olgaParser::ArrayDefinitionContext>(0);
}


size_t olgaParser::DefinitionContext::getRuleIndex() const {
  return olgaParser::RuleDefinition;
}


antlrcpp::Any olgaParser::DefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitDefinition(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::DefinitionContext* olgaParser::definition() {
  DefinitionContext *_localctx = _tracker.createInstance<DefinitionContext>(_ctx, getState());
  enterRule(_localctx, 20, olgaParser::RuleDefinition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(192);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 17, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(189);
      pointerDefinition();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(190);
      variableDefinition();
      break;
    }

    case 3: {
      enterOuterAlt(_localctx, 3);
      setState(191);
      arrayDefinition();
      break;
    }

    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- SizeSpecifierContext ------------------------------------------------------------------

olgaParser::SizeSpecifierContext::SizeSpecifierContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::U8() {
  return getToken(olgaParser::U8, 0);
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::U16() {
  return getToken(olgaParser::U16, 0);
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::U32() {
  return getToken(olgaParser::U32, 0);
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::I8() {
  return getToken(olgaParser::I8, 0);
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::I16() {
  return getToken(olgaParser::I16, 0);
}

tree::TerminalNode* olgaParser::SizeSpecifierContext::I32() {
  return getToken(olgaParser::I32, 0);
}


size_t olgaParser::SizeSpecifierContext::getRuleIndex() const {
  return olgaParser::RuleSizeSpecifier;
}


antlrcpp::Any olgaParser::SizeSpecifierContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitSizeSpecifier(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::SizeSpecifierContext* olgaParser::sizeSpecifier() {
  SizeSpecifierContext *_localctx = _tracker.createInstance<SizeSpecifierContext>(_ctx, getState());
  enterRule(_localctx, 22, olgaParser::RuleSizeSpecifier);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(194);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::U8)
      | (1ULL << olgaParser::I8)
      | (1ULL << olgaParser::U16)
      | (1ULL << olgaParser::I16)
      | (1ULL << olgaParser::U32)
      | (1ULL << olgaParser::I32))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PointerDeclarationContext ------------------------------------------------------------------

olgaParser::PointerDeclarationContext::PointerDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::PointerDeclarationContext::Ptr() {
  return getToken(olgaParser::Ptr, 0);
}

tree::TerminalNode* olgaParser::PointerDeclarationContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::SizeSpecifierContext* olgaParser::PointerDeclarationContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}


size_t olgaParser::PointerDeclarationContext::getRuleIndex() const {
  return olgaParser::RulePointerDeclaration;
}


antlrcpp::Any olgaParser::PointerDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitPointerDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::PointerDeclarationContext* olgaParser::pointerDeclaration() {
  PointerDeclarationContext *_localctx = _tracker.createInstance<PointerDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 24, olgaParser::RulePointerDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(196);
    match(olgaParser::Ptr);
    setState(197);
    match(olgaParser::Identifier);
    setState(198);
    match(olgaParser::T__7);
    setState(199);
    match(olgaParser::T__8);
    setState(200);
    sizeSpecifier();
    setState(201);
    match(olgaParser::T__9);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDeclarationContext ------------------------------------------------------------------

olgaParser::VariableDeclarationContext::VariableDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::VariableDeclarationContext::Let() {
  return getToken(olgaParser::Let, 0);
}

tree::TerminalNode* olgaParser::VariableDeclarationContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::SizeSpecifierContext* olgaParser::VariableDeclarationContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}


size_t olgaParser::VariableDeclarationContext::getRuleIndex() const {
  return olgaParser::RuleVariableDeclaration;
}


antlrcpp::Any olgaParser::VariableDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitVariableDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::VariableDeclarationContext* olgaParser::variableDeclaration() {
  VariableDeclarationContext *_localctx = _tracker.createInstance<VariableDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 26, olgaParser::RuleVariableDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(203);
    match(olgaParser::Let);
    setState(204);
    match(olgaParser::Identifier);
    setState(205);
    match(olgaParser::T__7);
    setState(206);
    match(olgaParser::T__8);
    setState(207);
    sizeSpecifier();
    setState(208);
    match(olgaParser::T__9);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayDeclarationContext ------------------------------------------------------------------

olgaParser::ArrayDeclarationContext::ArrayDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::ArrayDeclarationContext::Let() {
  return getToken(olgaParser::Let, 0);
}

tree::TerminalNode* olgaParser::ArrayDeclarationContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::SizeSpecifierContext* olgaParser::ArrayDeclarationContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}

tree::TerminalNode* olgaParser::ArrayDeclarationContext::Literal() {
  return getToken(olgaParser::Literal, 0);
}


size_t olgaParser::ArrayDeclarationContext::getRuleIndex() const {
  return olgaParser::RuleArrayDeclaration;
}


antlrcpp::Any olgaParser::ArrayDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitArrayDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ArrayDeclarationContext* olgaParser::arrayDeclaration() {
  ArrayDeclarationContext *_localctx = _tracker.createInstance<ArrayDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 28, olgaParser::RuleArrayDeclaration);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(210);
    match(olgaParser::Let);
    setState(211);
    match(olgaParser::Identifier);
    setState(212);
    match(olgaParser::T__7);
    setState(213);
    match(olgaParser::T__8);
    setState(214);
    sizeSpecifier();
    setState(215);
    match(olgaParser::T__6);
    setState(216);
    match(olgaParser::Literal);
    setState(217);
    match(olgaParser::T__9);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PointerDefinitionContext ------------------------------------------------------------------

olgaParser::PointerDefinitionContext::PointerDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::PointerDeclarationContext* olgaParser::PointerDefinitionContext::pointerDeclaration() {
  return getRuleContext<olgaParser::PointerDeclarationContext>(0);
}

tree::TerminalNode* olgaParser::PointerDefinitionContext::Literal() {
  return getToken(olgaParser::Literal, 0);
}

tree::TerminalNode* olgaParser::PointerDefinitionContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::PrefixOperatorContext* olgaParser::PointerDefinitionContext::prefixOperator() {
  return getRuleContext<olgaParser::PrefixOperatorContext>(0);
}


size_t olgaParser::PointerDefinitionContext::getRuleIndex() const {
  return olgaParser::RulePointerDefinition;
}


antlrcpp::Any olgaParser::PointerDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitPointerDefinition(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::PointerDefinitionContext* olgaParser::pointerDefinition() {
  PointerDefinitionContext *_localctx = _tracker.createInstance<PointerDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 30, olgaParser::RulePointerDefinition);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(219);
    pointerDeclaration();
    setState(220);
    match(olgaParser::T__10);
    setState(226);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case olgaParser::T__12:
      case olgaParser::T__13:
      case olgaParser::T__14:
      case olgaParser::Identifier: {
        setState(222);
        _errHandler->sync(this);

        _la = _input->LA(1);
        if ((((_la & ~ 0x3fULL) == 0) &&
          ((1ULL << _la) & ((1ULL << olgaParser::T__12)
          | (1ULL << olgaParser::T__13)
          | (1ULL << olgaParser::T__14))) != 0)) {
          setState(221);
          prefixOperator();
        }
        setState(224);
        match(olgaParser::Identifier);
        break;
      }

      case olgaParser::Literal: {
        setState(225);
        match(olgaParser::Literal);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- VariableDefinitionContext ------------------------------------------------------------------

olgaParser::VariableDefinitionContext::VariableDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::VariableDeclarationContext* olgaParser::VariableDefinitionContext::variableDeclaration() {
  return getRuleContext<olgaParser::VariableDeclarationContext>(0);
}

olgaParser::ExpressionContext* olgaParser::VariableDefinitionContext::expression() {
  return getRuleContext<olgaParser::ExpressionContext>(0);
}


size_t olgaParser::VariableDefinitionContext::getRuleIndex() const {
  return olgaParser::RuleVariableDefinition;
}


antlrcpp::Any olgaParser::VariableDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitVariableDefinition(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::VariableDefinitionContext* olgaParser::variableDefinition() {
  VariableDefinitionContext *_localctx = _tracker.createInstance<VariableDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 32, olgaParser::RuleVariableDefinition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(228);
    variableDeclaration();
    setState(229);
    match(olgaParser::T__10);
    setState(230);
    expression();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArrayDefinitionContext ------------------------------------------------------------------

olgaParser::ArrayDefinitionContext::ArrayDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::ArrayDeclarationContext* olgaParser::ArrayDefinitionContext::arrayDeclaration() {
  return getRuleContext<olgaParser::ArrayDeclarationContext>(0);
}

olgaParser::InitializerContext* olgaParser::ArrayDefinitionContext::initializer() {
  return getRuleContext<olgaParser::InitializerContext>(0);
}


size_t olgaParser::ArrayDefinitionContext::getRuleIndex() const {
  return olgaParser::RuleArrayDefinition;
}


antlrcpp::Any olgaParser::ArrayDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitArrayDefinition(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ArrayDefinitionContext* olgaParser::arrayDefinition() {
  ArrayDefinitionContext *_localctx = _tracker.createInstance<ArrayDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 34, olgaParser::RuleArrayDefinition);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(232);
    arrayDeclaration();
    setState(233);
    match(olgaParser::T__10);
    setState(234);
    initializer();
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- InitializerContext ------------------------------------------------------------------

olgaParser::InitializerContext::InitializerContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::SizeSpecifierContext* olgaParser::InitializerContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}

std::vector<olgaParser::ExpressionContext *> olgaParser::InitializerContext::expression() {
  return getRuleContexts<olgaParser::ExpressionContext>();
}

olgaParser::ExpressionContext* olgaParser::InitializerContext::expression(size_t i) {
  return getRuleContext<olgaParser::ExpressionContext>(i);
}


size_t olgaParser::InitializerContext::getRuleIndex() const {
  return olgaParser::RuleInitializer;
}


antlrcpp::Any olgaParser::InitializerContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitInitializer(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::InitializerContext* olgaParser::initializer() {
  InitializerContext *_localctx = _tracker.createInstance<InitializerContext>(_ctx, getState());
  enterRule(_localctx, 36, olgaParser::RuleInitializer);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(236);
    match(olgaParser::T__8);
    setState(237);
    sizeSpecifier();
    setState(238);
    match(olgaParser::T__9);
    setState(239);
    match(olgaParser::T__3);
    setState(241);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__8)
      | (1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::Identifier)
      | (1ULL << olgaParser::Literal))) != 0)) {
      setState(240);
      expression();
    }
    setState(247);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == olgaParser::T__6) {
      setState(243);
      match(olgaParser::T__6);
      setState(244);
      expression();
      setState(249);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(250);
    match(olgaParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- FunctionDefinitionContext ------------------------------------------------------------------

olgaParser::FunctionDefinitionContext::FunctionDefinitionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::FunctionDefinitionContext::Fn() {
  return getToken(olgaParser::Fn, 0);
}

tree::TerminalNode* olgaParser::FunctionDefinitionContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::ParameterDeclarationListContext* olgaParser::FunctionDefinitionContext::parameterDeclarationList() {
  return getRuleContext<olgaParser::ParameterDeclarationListContext>(0);
}

olgaParser::SizeSpecifierContext* olgaParser::FunctionDefinitionContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}

std::vector<olgaParser::CompoundStatementContext *> olgaParser::FunctionDefinitionContext::compoundStatement() {
  return getRuleContexts<olgaParser::CompoundStatementContext>();
}

olgaParser::CompoundStatementContext* olgaParser::FunctionDefinitionContext::compoundStatement(size_t i) {
  return getRuleContext<olgaParser::CompoundStatementContext>(i);
}


size_t olgaParser::FunctionDefinitionContext::getRuleIndex() const {
  return olgaParser::RuleFunctionDefinition;
}


antlrcpp::Any olgaParser::FunctionDefinitionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitFunctionDefinition(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::FunctionDefinitionContext* olgaParser::functionDefinition() {
  FunctionDefinitionContext *_localctx = _tracker.createInstance<FunctionDefinitionContext>(_ctx, getState());
  enterRule(_localctx, 38, olgaParser::RuleFunctionDefinition);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(252);
    match(olgaParser::Fn);
    setState(253);
    match(olgaParser::Identifier);
    setState(254);
    match(olgaParser::T__1);
    setState(256);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == olgaParser::Identifier) {
      setState(255);
      parameterDeclarationList();
    }
    setState(258);
    match(olgaParser::T__2);
    setState(261);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == olgaParser::T__11) {
      setState(259);
      match(olgaParser::T__11);
      setState(260);
      sizeSpecifier();
    }
    setState(263);
    match(olgaParser::T__3);
    setState(267);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__14)
      | (1ULL << olgaParser::If)
      | (1ULL << olgaParser::While)
      | (1ULL << olgaParser::Let)
      | (1ULL << olgaParser::Ptr)
      | (1ULL << olgaParser::Break)
      | (1ULL << olgaParser::Continue)
      | (1ULL << olgaParser::Return)
      | (1ULL << olgaParser::Identifier))) != 0)) {
      setState(264);
      compoundStatement();
      setState(269);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(270);
    match(olgaParser::T__4);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterDeclarationListContext ------------------------------------------------------------------

olgaParser::ParameterDeclarationListContext::ParameterDeclarationListContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<olgaParser::ParameterDeclarationContext *> olgaParser::ParameterDeclarationListContext::parameterDeclaration() {
  return getRuleContexts<olgaParser::ParameterDeclarationContext>();
}

olgaParser::ParameterDeclarationContext* olgaParser::ParameterDeclarationListContext::parameterDeclaration(size_t i) {
  return getRuleContext<olgaParser::ParameterDeclarationContext>(i);
}


size_t olgaParser::ParameterDeclarationListContext::getRuleIndex() const {
  return olgaParser::RuleParameterDeclarationList;
}


antlrcpp::Any olgaParser::ParameterDeclarationListContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitParameterDeclarationList(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ParameterDeclarationListContext* olgaParser::parameterDeclarationList() {
  ParameterDeclarationListContext *_localctx = _tracker.createInstance<ParameterDeclarationListContext>(_ctx, getState());
  enterRule(_localctx, 40, olgaParser::RuleParameterDeclarationList);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(272);
    parameterDeclaration();
    setState(277);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == olgaParser::T__6) {
      setState(273);
      match(olgaParser::T__6);
      setState(274);
      parameterDeclaration();
      setState(279);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ParameterDeclarationContext ------------------------------------------------------------------

olgaParser::ParameterDeclarationContext::ParameterDeclarationContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* olgaParser::ParameterDeclarationContext::Identifier() {
  return getToken(olgaParser::Identifier, 0);
}

olgaParser::SizeSpecifierContext* olgaParser::ParameterDeclarationContext::sizeSpecifier() {
  return getRuleContext<olgaParser::SizeSpecifierContext>(0);
}

tree::TerminalNode* olgaParser::ParameterDeclarationContext::Literal() {
  return getToken(olgaParser::Literal, 0);
}


size_t olgaParser::ParameterDeclarationContext::getRuleIndex() const {
  return olgaParser::RuleParameterDeclaration;
}


antlrcpp::Any olgaParser::ParameterDeclarationContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitParameterDeclaration(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ParameterDeclarationContext* olgaParser::parameterDeclaration() {
  ParameterDeclarationContext *_localctx = _tracker.createInstance<ParameterDeclarationContext>(_ctx, getState());
  enterRule(_localctx, 42, olgaParser::RuleParameterDeclaration);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(280);
    match(olgaParser::Identifier);
    setState(281);
    match(olgaParser::T__7);
    setState(282);
    match(olgaParser::T__8);
    setState(283);
    sizeSpecifier();
    setState(286);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == olgaParser::T__6) {
      setState(284);
      match(olgaParser::T__6);
      setState(285);
      match(olgaParser::Literal);
    }
    setState(288);
    match(olgaParser::T__9);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PrefixOperatorContext ------------------------------------------------------------------

olgaParser::PrefixOperatorContext::PrefixOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::NegationOperatorContext* olgaParser::PrefixOperatorContext::negationOperator() {
  return getRuleContext<olgaParser::NegationOperatorContext>(0);
}

olgaParser::DereferenceOperatorContext* olgaParser::PrefixOperatorContext::dereferenceOperator() {
  return getRuleContext<olgaParser::DereferenceOperatorContext>(0);
}

olgaParser::ReferenceOperatorContext* olgaParser::PrefixOperatorContext::referenceOperator() {
  return getRuleContext<olgaParser::ReferenceOperatorContext>(0);
}


size_t olgaParser::PrefixOperatorContext::getRuleIndex() const {
  return olgaParser::RulePrefixOperator;
}


antlrcpp::Any olgaParser::PrefixOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitPrefixOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::PrefixOperatorContext* olgaParser::prefixOperator() {
  PrefixOperatorContext *_localctx = _tracker.createInstance<PrefixOperatorContext>(_ctx, getState());
  enterRule(_localctx, 44, olgaParser::RulePrefixOperator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(293);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case olgaParser::T__14: {
        enterOuterAlt(_localctx, 1);
        setState(290);
        negationOperator();
        break;
      }

      case olgaParser::T__12: {
        enterOuterAlt(_localctx, 2);
        setState(291);
        dereferenceOperator();
        break;
      }

      case olgaParser::T__13: {
        enterOuterAlt(_localctx, 3);
        setState(292);
        referenceOperator();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- DereferenceOperatorContext ------------------------------------------------------------------

olgaParser::DereferenceOperatorContext::DereferenceOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::DereferenceOperatorContext::getRuleIndex() const {
  return olgaParser::RuleDereferenceOperator;
}


antlrcpp::Any olgaParser::DereferenceOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitDereferenceOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::DereferenceOperatorContext* olgaParser::dereferenceOperator() {
  DereferenceOperatorContext *_localctx = _tracker.createInstance<DereferenceOperatorContext>(_ctx, getState());
  enterRule(_localctx, 46, olgaParser::RuleDereferenceOperator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(295);
    match(olgaParser::T__12);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ReferenceOperatorContext ------------------------------------------------------------------

olgaParser::ReferenceOperatorContext::ReferenceOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::ReferenceOperatorContext::getRuleIndex() const {
  return olgaParser::RuleReferenceOperator;
}


antlrcpp::Any olgaParser::ReferenceOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitReferenceOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ReferenceOperatorContext* olgaParser::referenceOperator() {
  ReferenceOperatorContext *_localctx = _tracker.createInstance<ReferenceOperatorContext>(_ctx, getState());
  enterRule(_localctx, 48, olgaParser::RuleReferenceOperator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(297);
    match(olgaParser::T__13);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NegationOperatorContext ------------------------------------------------------------------

olgaParser::NegationOperatorContext::NegationOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::NegationOperatorContext::getRuleIndex() const {
  return olgaParser::RuleNegationOperator;
}


antlrcpp::Any olgaParser::NegationOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitNegationOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::NegationOperatorContext* olgaParser::negationOperator() {
  NegationOperatorContext *_localctx = _tracker.createInstance<NegationOperatorContext>(_ctx, getState());
  enterRule(_localctx, 50, olgaParser::RuleNegationOperator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(299);
    match(olgaParser::T__14);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssignmentOperatorContext ------------------------------------------------------------------

olgaParser::AssignmentOperatorContext::AssignmentOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::AssignmentOperatorContext::getRuleIndex() const {
  return olgaParser::RuleAssignmentOperator;
}


antlrcpp::Any olgaParser::AssignmentOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitAssignmentOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::AssignmentOperatorContext* olgaParser::assignmentOperator() {
  AssignmentOperatorContext *_localctx = _tracker.createInstance<AssignmentOperatorContext>(_ctx, getState());
  enterRule(_localctx, 52, olgaParser::RuleAssignmentOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(301);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__10)
      | (1ULL << olgaParser::T__15)
      | (1ULL << olgaParser::T__16)
      | (1ULL << olgaParser::T__17)
      | (1ULL << olgaParser::T__18))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- R_operatorContext ------------------------------------------------------------------

olgaParser::R_operatorContext::R_operatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::ArithmeticalOperatorContext* olgaParser::R_operatorContext::arithmeticalOperator() {
  return getRuleContext<olgaParser::ArithmeticalOperatorContext>(0);
}

olgaParser::LogicalOperatorContext* olgaParser::R_operatorContext::logicalOperator() {
  return getRuleContext<olgaParser::LogicalOperatorContext>(0);
}

olgaParser::RelationalOperatorContext* olgaParser::R_operatorContext::relationalOperator() {
  return getRuleContext<olgaParser::RelationalOperatorContext>(0);
}

olgaParser::ShiftOperatorContext* olgaParser::R_operatorContext::shiftOperator() {
  return getRuleContext<olgaParser::ShiftOperatorContext>(0);
}

olgaParser::BitOperatorContext* olgaParser::R_operatorContext::bitOperator() {
  return getRuleContext<olgaParser::BitOperatorContext>(0);
}


size_t olgaParser::R_operatorContext::getRuleIndex() const {
  return olgaParser::RuleR_operator;
}


antlrcpp::Any olgaParser::R_operatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitR_operator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::R_operatorContext* olgaParser::r_operator() {
  R_operatorContext *_localctx = _tracker.createInstance<R_operatorContext>(_ctx, getState());
  enterRule(_localctx, 54, olgaParser::RuleR_operator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(308);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case olgaParser::T__12:
      case olgaParser::T__19:
      case olgaParser::T__20:
      case olgaParser::T__21:
      case olgaParser::T__22: {
        enterOuterAlt(_localctx, 1);
        setState(303);
        arithmeticalOperator();
        break;
      }

      case olgaParser::T__14:
      case olgaParser::T__23:
      case olgaParser::T__24: {
        enterOuterAlt(_localctx, 2);
        setState(304);
        logicalOperator();
        break;
      }

      case olgaParser::T__8:
      case olgaParser::T__9:
      case olgaParser::T__25:
      case olgaParser::T__26:
      case olgaParser::T__27:
      case olgaParser::T__28: {
        enterOuterAlt(_localctx, 3);
        setState(305);
        relationalOperator();
        break;
      }

      case olgaParser::T__29:
      case olgaParser::T__30: {
        enterOuterAlt(_localctx, 4);
        setState(306);
        shiftOperator();
        break;
      }

      case olgaParser::T__13:
      case olgaParser::T__31:
      case olgaParser::T__32: {
        enterOuterAlt(_localctx, 5);
        setState(307);
        bitOperator();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ArithmeticalOperatorContext ------------------------------------------------------------------

olgaParser::ArithmeticalOperatorContext::ArithmeticalOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::ArithmeticalOperatorContext::getRuleIndex() const {
  return olgaParser::RuleArithmeticalOperator;
}


antlrcpp::Any olgaParser::ArithmeticalOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitArithmeticalOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ArithmeticalOperatorContext* olgaParser::arithmeticalOperator() {
  ArithmeticalOperatorContext *_localctx = _tracker.createInstance<ArithmeticalOperatorContext>(_ctx, getState());
  enterRule(_localctx, 56, olgaParser::RuleArithmeticalOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(310);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__12)
      | (1ULL << olgaParser::T__19)
      | (1ULL << olgaParser::T__20)
      | (1ULL << olgaParser::T__21)
      | (1ULL << olgaParser::T__22))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- LogicalOperatorContext ------------------------------------------------------------------

olgaParser::LogicalOperatorContext::LogicalOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

olgaParser::NegationOperatorContext* olgaParser::LogicalOperatorContext::negationOperator() {
  return getRuleContext<olgaParser::NegationOperatorContext>(0);
}


size_t olgaParser::LogicalOperatorContext::getRuleIndex() const {
  return olgaParser::RuleLogicalOperator;
}


antlrcpp::Any olgaParser::LogicalOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitLogicalOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::LogicalOperatorContext* olgaParser::logicalOperator() {
  LogicalOperatorContext *_localctx = _tracker.createInstance<LogicalOperatorContext>(_ctx, getState());
  enterRule(_localctx, 58, olgaParser::RuleLogicalOperator);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(315);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case olgaParser::T__23: {
        enterOuterAlt(_localctx, 1);
        setState(312);
        match(olgaParser::T__23);
        break;
      }

      case olgaParser::T__24: {
        enterOuterAlt(_localctx, 2);
        setState(313);
        match(olgaParser::T__24);
        break;
      }

      case olgaParser::T__14: {
        enterOuterAlt(_localctx, 3);
        setState(314);
        negationOperator();
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- RelationalOperatorContext ------------------------------------------------------------------

olgaParser::RelationalOperatorContext::RelationalOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::RelationalOperatorContext::getRuleIndex() const {
  return olgaParser::RuleRelationalOperator;
}


antlrcpp::Any olgaParser::RelationalOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitRelationalOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::RelationalOperatorContext* olgaParser::relationalOperator() {
  RelationalOperatorContext *_localctx = _tracker.createInstance<RelationalOperatorContext>(_ctx, getState());
  enterRule(_localctx, 60, olgaParser::RuleRelationalOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(317);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__8)
      | (1ULL << olgaParser::T__9)
      | (1ULL << olgaParser::T__25)
      | (1ULL << olgaParser::T__26)
      | (1ULL << olgaParser::T__27)
      | (1ULL << olgaParser::T__28))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ShiftOperatorContext ------------------------------------------------------------------

olgaParser::ShiftOperatorContext::ShiftOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::ShiftOperatorContext::getRuleIndex() const {
  return olgaParser::RuleShiftOperator;
}


antlrcpp::Any olgaParser::ShiftOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitShiftOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::ShiftOperatorContext* olgaParser::shiftOperator() {
  ShiftOperatorContext *_localctx = _tracker.createInstance<ShiftOperatorContext>(_ctx, getState());
  enterRule(_localctx, 62, olgaParser::RuleShiftOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(319);
    _la = _input->LA(1);
    if (!(_la == olgaParser::T__29

    || _la == olgaParser::T__30)) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- BitOperatorContext ------------------------------------------------------------------

olgaParser::BitOperatorContext::BitOperatorContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t olgaParser::BitOperatorContext::getRuleIndex() const {
  return olgaParser::RuleBitOperator;
}


antlrcpp::Any olgaParser::BitOperatorContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<olgaVisitor*>(visitor))
    return parserVisitor->visitBitOperator(this);
  else
    return visitor->visitChildren(this);
}

olgaParser::BitOperatorContext* olgaParser::bitOperator() {
  BitOperatorContext *_localctx = _tracker.createInstance<BitOperatorContext>(_ctx, getState());
  enterRule(_localctx, 64, olgaParser::RuleBitOperator);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(321);
    _la = _input->LA(1);
    if (!((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << olgaParser::T__13)
      | (1ULL << olgaParser::T__31)
      | (1ULL << olgaParser::T__32))) != 0))) {
    _errHandler->recoverInline(this);
    }
    else {
      _errHandler->reportMatch(this);
      consume();
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> olgaParser::_decisionToDFA;
atn::PredictionContextCache olgaParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN olgaParser::_atn;
std::vector<uint16_t> olgaParser::_serializedATN;

std::vector<std::string> olgaParser::_ruleNames = {
  "compilationUnit", "externalDeclaration", "compoundStatement", "jumpStatement", 
  "ifStatement", "whileStatement", "functionCall", "expression", "assignmentExpression", 
  "declaration", "definition", "sizeSpecifier", "pointerDeclaration", "variableDeclaration", 
  "arrayDeclaration", "pointerDefinition", "variableDefinition", "arrayDefinition", 
  "initializer", "functionDefinition", "parameterDeclarationList", "parameterDeclaration", 
  "prefixOperator", "dereferenceOperator", "referenceOperator", "negationOperator", 
  "assignmentOperator", "r_operator", "arithmeticalOperator", "logicalOperator", 
  "relationalOperator", "shiftOperator", "bitOperator"
};

std::vector<std::string> olgaParser::_literalNames = {
  "", "';'", "'('", "')'", "'{'", "'}'", "'else'", "','", "':'", "'<'", 
  "'>'", "'='", "'->'", "'*'", "'&'", "'!'", "'+='", "'-='", "'*='", "'/='", 
  "'+'", "'-'", "'/'", "'%'", "'||'", "'&&'", "'<='", "'>='", "'!='", "'=='", 
  "'<<'", "'>>'", "'|'", "'^'", "'if'", "'while'", "'let'", "'ptr'", "'break'", 
  "'continue'", "'return'", "'fn'", "'u8'", "'i8'", "'u16'", "'i16'", "'u32'", 
  "'i32'"
};

std::vector<std::string> olgaParser::_symbolicNames = {
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 
  "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "If", 
  "While", "Let", "Ptr", "Break", "Continue", "Return", "Fn", "U8", "I8", 
  "U16", "I16", "U32", "I32", "Identifier", "Literal", "NumericLiteral", 
  "StringLiteral", "WS", "LINE_COMMENT"
};

dfa::Vocabulary olgaParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> olgaParser::_tokenNames;

olgaParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0x37, 0x146, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
    0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 
    0x4, 0x8, 0x9, 0x8, 0x4, 0x9, 0x9, 0x9, 0x4, 0xa, 0x9, 0xa, 0x4, 0xb, 
    0x9, 0xb, 0x4, 0xc, 0x9, 0xc, 0x4, 0xd, 0x9, 0xd, 0x4, 0xe, 0x9, 0xe, 
    0x4, 0xf, 0x9, 0xf, 0x4, 0x10, 0x9, 0x10, 0x4, 0x11, 0x9, 0x11, 0x4, 
    0x12, 0x9, 0x12, 0x4, 0x13, 0x9, 0x13, 0x4, 0x14, 0x9, 0x14, 0x4, 0x15, 
    0x9, 0x15, 0x4, 0x16, 0x9, 0x16, 0x4, 0x17, 0x9, 0x17, 0x4, 0x18, 0x9, 
    0x18, 0x4, 0x19, 0x9, 0x19, 0x4, 0x1a, 0x9, 0x1a, 0x4, 0x1b, 0x9, 0x1b, 
    0x4, 0x1c, 0x9, 0x1c, 0x4, 0x1d, 0x9, 0x1d, 0x4, 0x1e, 0x9, 0x1e, 0x4, 
    0x1f, 0x9, 0x1f, 0x4, 0x20, 0x9, 0x20, 0x4, 0x21, 0x9, 0x21, 0x4, 0x22, 
    0x9, 0x22, 0x3, 0x2, 0x6, 0x2, 0x46, 0xa, 0x2, 0xd, 0x2, 0xe, 0x2, 0x47, 
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 
    0x5, 0x3, 0x51, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x5, 0x4, 0x62, 0xa, 0x4, 0x3, 0x5, 
    0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 
    0x6b, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
    0x6, 0x7, 0x6, 0x73, 0xa, 0x6, 0xc, 0x6, 0xe, 0x6, 0x76, 0xb, 0x6, 0x3, 
    0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x7, 0x6, 0x7c, 0xa, 0x6, 0xc, 0x6, 
    0xe, 0x6, 0x7f, 0xb, 0x6, 0x3, 0x6, 0x5, 0x6, 0x82, 0xa, 0x6, 0x3, 0x7, 
    0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x7, 0x7, 0x7, 0x8a, 0xa, 
    0x7, 0xc, 0x7, 0xe, 0x7, 0x8d, 0xb, 0x7, 0x3, 0x7, 0x3, 0x7, 0x3, 0x8, 
    0x3, 0x8, 0x3, 0x8, 0x5, 0x8, 0x94, 0xa, 0x8, 0x3, 0x8, 0x3, 0x8, 0x7, 
    0x8, 0x98, 0xa, 0x8, 0xc, 0x8, 0xe, 0x8, 0x9b, 0xb, 0x8, 0x3, 0x8, 0x3, 
    0x8, 0x3, 0x9, 0x5, 0x9, 0xa0, 0xa, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 
    0x3, 0x9, 0x5, 0x9, 0xa6, 0xa, 0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 0x9, 0xaa, 
    0xa, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x3, 0x9, 0x5, 0x9, 0xb0, 0xa, 
    0x9, 0x5, 0x9, 0xb2, 0xa, 0x9, 0x3, 0xa, 0x5, 0xa, 0xb5, 0xa, 0xa, 0x3, 
    0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xa, 0x3, 0xb, 0x3, 0xb, 0x3, 0xb, 0x5, 
    0xb, 0xbe, 0xa, 0xb, 0x3, 0xc, 0x3, 0xc, 0x3, 0xc, 0x5, 0xc, 0xc3, 0xa, 
    0xc, 0x3, 0xd, 0x3, 0xd, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 
    0xe, 0x3, 0xe, 0x3, 0xe, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 
    0xf, 0x3, 0xf, 0x3, 0xf, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 
    0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x10, 0x3, 0x11, 0x3, 
    0x11, 0x3, 0x11, 0x5, 0x11, 0xe1, 0xa, 0x11, 0x3, 0x11, 0x3, 0x11, 0x5, 
    0x11, 0xe5, 0xa, 0x11, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 0x12, 0x3, 
    0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x13, 0x3, 0x14, 0x3, 0x14, 0x3, 0x14, 
    0x3, 0x14, 0x3, 0x14, 0x5, 0x14, 0xf4, 0xa, 0x14, 0x3, 0x14, 0x3, 0x14, 
    0x7, 0x14, 0xf8, 0xa, 0x14, 0xc, 0x14, 0xe, 0x14, 0xfb, 0xb, 0x14, 0x3, 
    0x14, 0x3, 0x14, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 0x15, 
    0x103, 0xa, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x15, 0x5, 0x15, 0x108, 
    0xa, 0x15, 0x3, 0x15, 0x3, 0x15, 0x7, 0x15, 0x10c, 0xa, 0x15, 0xc, 0x15, 
    0xe, 0x15, 0x10f, 0xb, 0x15, 0x3, 0x15, 0x3, 0x15, 0x3, 0x16, 0x3, 0x16, 
    0x3, 0x16, 0x7, 0x16, 0x116, 0xa, 0x16, 0xc, 0x16, 0xe, 0x16, 0x119, 
    0xb, 0x16, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 
    0x17, 0x5, 0x17, 0x121, 0xa, 0x17, 0x3, 0x17, 0x3, 0x17, 0x3, 0x18, 
    0x3, 0x18, 0x3, 0x18, 0x5, 0x18, 0x128, 0xa, 0x18, 0x3, 0x19, 0x3, 0x19, 
    0x3, 0x1a, 0x3, 0x1a, 0x3, 0x1b, 0x3, 0x1b, 0x3, 0x1c, 0x3, 0x1c, 0x3, 
    0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x3, 0x1d, 0x5, 0x1d, 0x137, 
    0xa, 0x1d, 0x3, 0x1e, 0x3, 0x1e, 0x3, 0x1f, 0x3, 0x1f, 0x3, 0x1f, 0x5, 
    0x1f, 0x13e, 0xa, 0x1f, 0x3, 0x20, 0x3, 0x20, 0x3, 0x21, 0x3, 0x21, 
    0x3, 0x22, 0x3, 0x22, 0x3, 0x22, 0x2, 0x2, 0x23, 0x2, 0x4, 0x6, 0x8, 
    0xa, 0xc, 0xe, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1a, 0x1c, 0x1e, 0x20, 
    0x22, 0x24, 0x26, 0x28, 0x2a, 0x2c, 0x2e, 0x30, 0x32, 0x34, 0x36, 0x38, 
    0x3a, 0x3c, 0x3e, 0x40, 0x42, 0x2, 0x9, 0x3, 0x2, 0x32, 0x33, 0x3, 0x2, 
    0x2c, 0x31, 0x4, 0x2, 0xd, 0xd, 0x12, 0x15, 0x4, 0x2, 0xf, 0xf, 0x16, 
    0x19, 0x4, 0x2, 0xb, 0xc, 0x1c, 0x1f, 0x3, 0x2, 0x20, 0x21, 0x4, 0x2, 
    0x10, 0x10, 0x22, 0x23, 0x2, 0x154, 0x2, 0x45, 0x3, 0x2, 0x2, 0x2, 0x4, 
    0x50, 0x3, 0x2, 0x2, 0x2, 0x6, 0x61, 0x3, 0x2, 0x2, 0x2, 0x8, 0x6a, 
    0x3, 0x2, 0x2, 0x2, 0xa, 0x6c, 0x3, 0x2, 0x2, 0x2, 0xc, 0x83, 0x3, 0x2, 
    0x2, 0x2, 0xe, 0x90, 0x3, 0x2, 0x2, 0x2, 0x10, 0x9f, 0x3, 0x2, 0x2, 
    0x2, 0x12, 0xb4, 0x3, 0x2, 0x2, 0x2, 0x14, 0xbd, 0x3, 0x2, 0x2, 0x2, 
    0x16, 0xc2, 0x3, 0x2, 0x2, 0x2, 0x18, 0xc4, 0x3, 0x2, 0x2, 0x2, 0x1a, 
    0xc6, 0x3, 0x2, 0x2, 0x2, 0x1c, 0xcd, 0x3, 0x2, 0x2, 0x2, 0x1e, 0xd4, 
    0x3, 0x2, 0x2, 0x2, 0x20, 0xdd, 0x3, 0x2, 0x2, 0x2, 0x22, 0xe6, 0x3, 
    0x2, 0x2, 0x2, 0x24, 0xea, 0x3, 0x2, 0x2, 0x2, 0x26, 0xee, 0x3, 0x2, 
    0x2, 0x2, 0x28, 0xfe, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x112, 0x3, 0x2, 0x2, 
    0x2, 0x2c, 0x11a, 0x3, 0x2, 0x2, 0x2, 0x2e, 0x127, 0x3, 0x2, 0x2, 0x2, 
    0x30, 0x129, 0x3, 0x2, 0x2, 0x2, 0x32, 0x12b, 0x3, 0x2, 0x2, 0x2, 0x34, 
    0x12d, 0x3, 0x2, 0x2, 0x2, 0x36, 0x12f, 0x3, 0x2, 0x2, 0x2, 0x38, 0x136, 
    0x3, 0x2, 0x2, 0x2, 0x3a, 0x138, 0x3, 0x2, 0x2, 0x2, 0x3c, 0x13d, 0x3, 
    0x2, 0x2, 0x2, 0x3e, 0x13f, 0x3, 0x2, 0x2, 0x2, 0x40, 0x141, 0x3, 0x2, 
    0x2, 0x2, 0x42, 0x143, 0x3, 0x2, 0x2, 0x2, 0x44, 0x46, 0x5, 0x4, 0x3, 
    0x2, 0x45, 0x44, 0x3, 0x2, 0x2, 0x2, 0x46, 0x47, 0x3, 0x2, 0x2, 0x2, 
    0x47, 0x45, 0x3, 0x2, 0x2, 0x2, 0x47, 0x48, 0x3, 0x2, 0x2, 0x2, 0x48, 
    0x3, 0x3, 0x2, 0x2, 0x2, 0x49, 0x51, 0x5, 0x28, 0x15, 0x2, 0x4a, 0x4b, 
    0x5, 0x14, 0xb, 0x2, 0x4b, 0x4c, 0x7, 0x3, 0x2, 0x2, 0x4c, 0x51, 0x3, 
    0x2, 0x2, 0x2, 0x4d, 0x4e, 0x5, 0x16, 0xc, 0x2, 0x4e, 0x4f, 0x7, 0x3, 
    0x2, 0x2, 0x4f, 0x51, 0x3, 0x2, 0x2, 0x2, 0x50, 0x49, 0x3, 0x2, 0x2, 
    0x2, 0x50, 0x4a, 0x3, 0x2, 0x2, 0x2, 0x50, 0x4d, 0x3, 0x2, 0x2, 0x2, 
    0x51, 0x5, 0x3, 0x2, 0x2, 0x2, 0x52, 0x62, 0x5, 0x8, 0x5, 0x2, 0x53, 
    0x54, 0x5, 0x14, 0xb, 0x2, 0x54, 0x55, 0x7, 0x3, 0x2, 0x2, 0x55, 0x62, 
    0x3, 0x2, 0x2, 0x2, 0x56, 0x57, 0x5, 0x16, 0xc, 0x2, 0x57, 0x58, 0x7, 
    0x3, 0x2, 0x2, 0x58, 0x62, 0x3, 0x2, 0x2, 0x2, 0x59, 0x5a, 0x5, 0x12, 
    0xa, 0x2, 0x5a, 0x5b, 0x7, 0x3, 0x2, 0x2, 0x5b, 0x62, 0x3, 0x2, 0x2, 
    0x2, 0x5c, 0x62, 0x5, 0xa, 0x6, 0x2, 0x5d, 0x62, 0x5, 0xc, 0x7, 0x2, 
    0x5e, 0x5f, 0x5, 0xe, 0x8, 0x2, 0x5f, 0x60, 0x7, 0x3, 0x2, 0x2, 0x60, 
    0x62, 0x3, 0x2, 0x2, 0x2, 0x61, 0x52, 0x3, 0x2, 0x2, 0x2, 0x61, 0x53, 
    0x3, 0x2, 0x2, 0x2, 0x61, 0x56, 0x3, 0x2, 0x2, 0x2, 0x61, 0x59, 0x3, 
    0x2, 0x2, 0x2, 0x61, 0x5c, 0x3, 0x2, 0x2, 0x2, 0x61, 0x5d, 0x3, 0x2, 
    0x2, 0x2, 0x61, 0x5e, 0x3, 0x2, 0x2, 0x2, 0x62, 0x7, 0x3, 0x2, 0x2, 
    0x2, 0x63, 0x64, 0x7, 0x2a, 0x2, 0x2, 0x64, 0x65, 0x9, 0x2, 0x2, 0x2, 
    0x65, 0x6b, 0x7, 0x3, 0x2, 0x2, 0x66, 0x67, 0x7, 0x28, 0x2, 0x2, 0x67, 
    0x6b, 0x7, 0x3, 0x2, 0x2, 0x68, 0x69, 0x7, 0x29, 0x2, 0x2, 0x69, 0x6b, 
    0x7, 0x3, 0x2, 0x2, 0x6a, 0x63, 0x3, 0x2, 0x2, 0x2, 0x6a, 0x66, 0x3, 
    0x2, 0x2, 0x2, 0x6a, 0x68, 0x3, 0x2, 0x2, 0x2, 0x6b, 0x9, 0x3, 0x2, 
    0x2, 0x2, 0x6c, 0x6d, 0x7, 0x24, 0x2, 0x2, 0x6d, 0x6e, 0x7, 0x4, 0x2, 
    0x2, 0x6e, 0x6f, 0x5, 0x10, 0x9, 0x2, 0x6f, 0x70, 0x7, 0x5, 0x2, 0x2, 
    0x70, 0x74, 0x7, 0x6, 0x2, 0x2, 0x71, 0x73, 0x5, 0x6, 0x4, 0x2, 0x72, 
    0x71, 0x3, 0x2, 0x2, 0x2, 0x73, 0x76, 0x3, 0x2, 0x2, 0x2, 0x74, 0x72, 
    0x3, 0x2, 0x2, 0x2, 0x74, 0x75, 0x3, 0x2, 0x2, 0x2, 0x75, 0x77, 0x3, 
    0x2, 0x2, 0x2, 0x76, 0x74, 0x3, 0x2, 0x2, 0x2, 0x77, 0x81, 0x7, 0x7, 
    0x2, 0x2, 0x78, 0x79, 0x7, 0x8, 0x2, 0x2, 0x79, 0x7d, 0x7, 0x6, 0x2, 
    0x2, 0x7a, 0x7c, 0x5, 0x6, 0x4, 0x2, 0x7b, 0x7a, 0x3, 0x2, 0x2, 0x2, 
    0x7c, 0x7f, 0x3, 0x2, 0x2, 0x2, 0x7d, 0x7b, 0x3, 0x2, 0x2, 0x2, 0x7d, 
    0x7e, 0x3, 0x2, 0x2, 0x2, 0x7e, 0x80, 0x3, 0x2, 0x2, 0x2, 0x7f, 0x7d, 
    0x3, 0x2, 0x2, 0x2, 0x80, 0x82, 0x7, 0x7, 0x2, 0x2, 0x81, 0x78, 0x3, 
    0x2, 0x2, 0x2, 0x81, 0x82, 0x3, 0x2, 0x2, 0x2, 0x82, 0xb, 0x3, 0x2, 
    0x2, 0x2, 0x83, 0x84, 0x7, 0x25, 0x2, 0x2, 0x84, 0x85, 0x7, 0x4, 0x2, 
    0x2, 0x85, 0x86, 0x5, 0x10, 0x9, 0x2, 0x86, 0x87, 0x7, 0x5, 0x2, 0x2, 
    0x87, 0x8b, 0x7, 0x6, 0x2, 0x2, 0x88, 0x8a, 0x5, 0x6, 0x4, 0x2, 0x89, 
    0x88, 0x3, 0x2, 0x2, 0x2, 0x8a, 0x8d, 0x3, 0x2, 0x2, 0x2, 0x8b, 0x89, 
    0x3, 0x2, 0x2, 0x2, 0x8b, 0x8c, 0x3, 0x2, 0x2, 0x2, 0x8c, 0x8e, 0x3, 
    0x2, 0x2, 0x2, 0x8d, 0x8b, 0x3, 0x2, 0x2, 0x2, 0x8e, 0x8f, 0x7, 0x7, 
    0x2, 0x2, 0x8f, 0xd, 0x3, 0x2, 0x2, 0x2, 0x90, 0x91, 0x7, 0x32, 0x2, 
    0x2, 0x91, 0x93, 0x7, 0x4, 0x2, 0x2, 0x92, 0x94, 0x5, 0x10, 0x9, 0x2, 
    0x93, 0x92, 0x3, 0x2, 0x2, 0x2, 0x93, 0x94, 0x3, 0x2, 0x2, 0x2, 0x94, 
    0x99, 0x3, 0x2, 0x2, 0x2, 0x95, 0x96, 0x7, 0x9, 0x2, 0x2, 0x96, 0x98, 
    0x5, 0x10, 0x9, 0x2, 0x97, 0x95, 0x3, 0x2, 0x2, 0x2, 0x98, 0x9b, 0x3, 
    0x2, 0x2, 0x2, 0x99, 0x97, 0x3, 0x2, 0x2, 0x2, 0x99, 0x9a, 0x3, 0x2, 
    0x2, 0x2, 0x9a, 0x9c, 0x3, 0x2, 0x2, 0x2, 0x9b, 0x99, 0x3, 0x2, 0x2, 
    0x2, 0x9c, 0x9d, 0x7, 0x5, 0x2, 0x2, 0x9d, 0xf, 0x3, 0x2, 0x2, 0x2, 
    0x9e, 0xa0, 0x5, 0x2e, 0x18, 0x2, 0x9f, 0x9e, 0x3, 0x2, 0x2, 0x2, 0x9f, 
    0xa0, 0x3, 0x2, 0x2, 0x2, 0xa0, 0xa5, 0x3, 0x2, 0x2, 0x2, 0xa1, 0xa6, 
    0x7, 0x32, 0x2, 0x2, 0xa2, 0xa6, 0x7, 0x33, 0x2, 0x2, 0xa3, 0xa6, 0x5, 
    0xe, 0x8, 0x2, 0xa4, 0xa6, 0x5, 0x26, 0x14, 0x2, 0xa5, 0xa1, 0x3, 0x2, 
    0x2, 0x2, 0xa5, 0xa2, 0x3, 0x2, 0x2, 0x2, 0xa5, 0xa3, 0x3, 0x2, 0x2, 
    0x2, 0xa5, 0xa4, 0x3, 0x2, 0x2, 0x2, 0xa6, 0xb1, 0x3, 0x2, 0x2, 0x2, 
    0xa7, 0xa9, 0x5, 0x38, 0x1d, 0x2, 0xa8, 0xaa, 0x5, 0x2e, 0x18, 0x2, 
    0xa9, 0xa8, 0x3, 0x2, 0x2, 0x2, 0xa9, 0xaa, 0x3, 0x2, 0x2, 0x2, 0xaa, 
    0xaf, 0x3, 0x2, 0x2, 0x2, 0xab, 0xb0, 0x7, 0x32, 0x2, 0x2, 0xac, 0xb0, 
    0x7, 0x33, 0x2, 0x2, 0xad, 0xb0, 0x5, 0xe, 0x8, 0x2, 0xae, 0xb0, 0x5, 
    0x26, 0x14, 0x2, 0xaf, 0xab, 0x3, 0x2, 0x2, 0x2, 0xaf, 0xac, 0x3, 0x2, 
    0x2, 0x2, 0xaf, 0xad, 0x3, 0x2, 0x2, 0x2, 0xaf, 0xae, 0x3, 0x2, 0x2, 
    0x2, 0xb0, 0xb2, 0x3, 0x2, 0x2, 0x2, 0xb1, 0xa7, 0x3, 0x2, 0x2, 0x2, 
    0xb1, 0xb2, 0x3, 0x2, 0x2, 0x2, 0xb2, 0x11, 0x3, 0x2, 0x2, 0x2, 0xb3, 
    0xb5, 0x5, 0x2e, 0x18, 0x2, 0xb4, 0xb3, 0x3, 0x2, 0x2, 0x2, 0xb4, 0xb5, 
    0x3, 0x2, 0x2, 0x2, 0xb5, 0xb6, 0x3, 0x2, 0x2, 0x2, 0xb6, 0xb7, 0x7, 
    0x32, 0x2, 0x2, 0xb7, 0xb8, 0x5, 0x36, 0x1c, 0x2, 0xb8, 0xb9, 0x5, 0x10, 
    0x9, 0x2, 0xb9, 0x13, 0x3, 0x2, 0x2, 0x2, 0xba, 0xbe, 0x5, 0x1a, 0xe, 
    0x2, 0xbb, 0xbe, 0x5, 0x1c, 0xf, 0x2, 0xbc, 0xbe, 0x5, 0x1e, 0x10, 0x2, 
    0xbd, 0xba, 0x3, 0x2, 0x2, 0x2, 0xbd, 0xbb, 0x3, 0x2, 0x2, 0x2, 0xbd, 
    0xbc, 0x3, 0x2, 0x2, 0x2, 0xbe, 0x15, 0x3, 0x2, 0x2, 0x2, 0xbf, 0xc3, 
    0x5, 0x20, 0x11, 0x2, 0xc0, 0xc3, 0x5, 0x22, 0x12, 0x2, 0xc1, 0xc3, 
    0x5, 0x24, 0x13, 0x2, 0xc2, 0xbf, 0x3, 0x2, 0x2, 0x2, 0xc2, 0xc0, 0x3, 
    0x2, 0x2, 0x2, 0xc2, 0xc1, 0x3, 0x2, 0x2, 0x2, 0xc3, 0x17, 0x3, 0x2, 
    0x2, 0x2, 0xc4, 0xc5, 0x9, 0x3, 0x2, 0x2, 0xc5, 0x19, 0x3, 0x2, 0x2, 
    0x2, 0xc6, 0xc7, 0x7, 0x27, 0x2, 0x2, 0xc7, 0xc8, 0x7, 0x32, 0x2, 0x2, 
    0xc8, 0xc9, 0x7, 0xa, 0x2, 0x2, 0xc9, 0xca, 0x7, 0xb, 0x2, 0x2, 0xca, 
    0xcb, 0x5, 0x18, 0xd, 0x2, 0xcb, 0xcc, 0x7, 0xc, 0x2, 0x2, 0xcc, 0x1b, 
    0x3, 0x2, 0x2, 0x2, 0xcd, 0xce, 0x7, 0x26, 0x2, 0x2, 0xce, 0xcf, 0x7, 
    0x32, 0x2, 0x2, 0xcf, 0xd0, 0x7, 0xa, 0x2, 0x2, 0xd0, 0xd1, 0x7, 0xb, 
    0x2, 0x2, 0xd1, 0xd2, 0x5, 0x18, 0xd, 0x2, 0xd2, 0xd3, 0x7, 0xc, 0x2, 
    0x2, 0xd3, 0x1d, 0x3, 0x2, 0x2, 0x2, 0xd4, 0xd5, 0x7, 0x26, 0x2, 0x2, 
    0xd5, 0xd6, 0x7, 0x32, 0x2, 0x2, 0xd6, 0xd7, 0x7, 0xa, 0x2, 0x2, 0xd7, 
    0xd8, 0x7, 0xb, 0x2, 0x2, 0xd8, 0xd9, 0x5, 0x18, 0xd, 0x2, 0xd9, 0xda, 
    0x7, 0x9, 0x2, 0x2, 0xda, 0xdb, 0x7, 0x33, 0x2, 0x2, 0xdb, 0xdc, 0x7, 
    0xc, 0x2, 0x2, 0xdc, 0x1f, 0x3, 0x2, 0x2, 0x2, 0xdd, 0xde, 0x5, 0x1a, 
    0xe, 0x2, 0xde, 0xe4, 0x7, 0xd, 0x2, 0x2, 0xdf, 0xe1, 0x5, 0x2e, 0x18, 
    0x2, 0xe0, 0xdf, 0x3, 0x2, 0x2, 0x2, 0xe0, 0xe1, 0x3, 0x2, 0x2, 0x2, 
    0xe1, 0xe2, 0x3, 0x2, 0x2, 0x2, 0xe2, 0xe5, 0x7, 0x32, 0x2, 0x2, 0xe3, 
    0xe5, 0x7, 0x33, 0x2, 0x2, 0xe4, 0xe0, 0x3, 0x2, 0x2, 0x2, 0xe4, 0xe3, 
    0x3, 0x2, 0x2, 0x2, 0xe5, 0x21, 0x3, 0x2, 0x2, 0x2, 0xe6, 0xe7, 0x5, 
    0x1c, 0xf, 0x2, 0xe7, 0xe8, 0x7, 0xd, 0x2, 0x2, 0xe8, 0xe9, 0x5, 0x10, 
    0x9, 0x2, 0xe9, 0x23, 0x3, 0x2, 0x2, 0x2, 0xea, 0xeb, 0x5, 0x1e, 0x10, 
    0x2, 0xeb, 0xec, 0x7, 0xd, 0x2, 0x2, 0xec, 0xed, 0x5, 0x26, 0x14, 0x2, 
    0xed, 0x25, 0x3, 0x2, 0x2, 0x2, 0xee, 0xef, 0x7, 0xb, 0x2, 0x2, 0xef, 
    0xf0, 0x5, 0x18, 0xd, 0x2, 0xf0, 0xf1, 0x7, 0xc, 0x2, 0x2, 0xf1, 0xf3, 
    0x7, 0x6, 0x2, 0x2, 0xf2, 0xf4, 0x5, 0x10, 0x9, 0x2, 0xf3, 0xf2, 0x3, 
    0x2, 0x2, 0x2, 0xf3, 0xf4, 0x3, 0x2, 0x2, 0x2, 0xf4, 0xf9, 0x3, 0x2, 
    0x2, 0x2, 0xf5, 0xf6, 0x7, 0x9, 0x2, 0x2, 0xf6, 0xf8, 0x5, 0x10, 0x9, 
    0x2, 0xf7, 0xf5, 0x3, 0x2, 0x2, 0x2, 0xf8, 0xfb, 0x3, 0x2, 0x2, 0x2, 
    0xf9, 0xf7, 0x3, 0x2, 0x2, 0x2, 0xf9, 0xfa, 0x3, 0x2, 0x2, 0x2, 0xfa, 
    0xfc, 0x3, 0x2, 0x2, 0x2, 0xfb, 0xf9, 0x3, 0x2, 0x2, 0x2, 0xfc, 0xfd, 
    0x7, 0x7, 0x2, 0x2, 0xfd, 0x27, 0x3, 0x2, 0x2, 0x2, 0xfe, 0xff, 0x7, 
    0x2b, 0x2, 0x2, 0xff, 0x100, 0x7, 0x32, 0x2, 0x2, 0x100, 0x102, 0x7, 
    0x4, 0x2, 0x2, 0x101, 0x103, 0x5, 0x2a, 0x16, 0x2, 0x102, 0x101, 0x3, 
    0x2, 0x2, 0x2, 0x102, 0x103, 0x3, 0x2, 0x2, 0x2, 0x103, 0x104, 0x3, 
    0x2, 0x2, 0x2, 0x104, 0x107, 0x7, 0x5, 0x2, 0x2, 0x105, 0x106, 0x7, 
    0xe, 0x2, 0x2, 0x106, 0x108, 0x5, 0x18, 0xd, 0x2, 0x107, 0x105, 0x3, 
    0x2, 0x2, 0x2, 0x107, 0x108, 0x3, 0x2, 0x2, 0x2, 0x108, 0x109, 0x3, 
    0x2, 0x2, 0x2, 0x109, 0x10d, 0x7, 0x6, 0x2, 0x2, 0x10a, 0x10c, 0x5, 
    0x6, 0x4, 0x2, 0x10b, 0x10a, 0x3, 0x2, 0x2, 0x2, 0x10c, 0x10f, 0x3, 
    0x2, 0x2, 0x2, 0x10d, 0x10b, 0x3, 0x2, 0x2, 0x2, 0x10d, 0x10e, 0x3, 
    0x2, 0x2, 0x2, 0x10e, 0x110, 0x3, 0x2, 0x2, 0x2, 0x10f, 0x10d, 0x3, 
    0x2, 0x2, 0x2, 0x110, 0x111, 0x7, 0x7, 0x2, 0x2, 0x111, 0x29, 0x3, 0x2, 
    0x2, 0x2, 0x112, 0x117, 0x5, 0x2c, 0x17, 0x2, 0x113, 0x114, 0x7, 0x9, 
    0x2, 0x2, 0x114, 0x116, 0x5, 0x2c, 0x17, 0x2, 0x115, 0x113, 0x3, 0x2, 
    0x2, 0x2, 0x116, 0x119, 0x3, 0x2, 0x2, 0x2, 0x117, 0x115, 0x3, 0x2, 
    0x2, 0x2, 0x117, 0x118, 0x3, 0x2, 0x2, 0x2, 0x118, 0x2b, 0x3, 0x2, 0x2, 
    0x2, 0x119, 0x117, 0x3, 0x2, 0x2, 0x2, 0x11a, 0x11b, 0x7, 0x32, 0x2, 
    0x2, 0x11b, 0x11c, 0x7, 0xa, 0x2, 0x2, 0x11c, 0x11d, 0x7, 0xb, 0x2, 
    0x2, 0x11d, 0x120, 0x5, 0x18, 0xd, 0x2, 0x11e, 0x11f, 0x7, 0x9, 0x2, 
    0x2, 0x11f, 0x121, 0x7, 0x33, 0x2, 0x2, 0x120, 0x11e, 0x3, 0x2, 0x2, 
    0x2, 0x120, 0x121, 0x3, 0x2, 0x2, 0x2, 0x121, 0x122, 0x3, 0x2, 0x2, 
    0x2, 0x122, 0x123, 0x7, 0xc, 0x2, 0x2, 0x123, 0x2d, 0x3, 0x2, 0x2, 0x2, 
    0x124, 0x128, 0x5, 0x34, 0x1b, 0x2, 0x125, 0x128, 0x5, 0x30, 0x19, 0x2, 
    0x126, 0x128, 0x5, 0x32, 0x1a, 0x2, 0x127, 0x124, 0x3, 0x2, 0x2, 0x2, 
    0x127, 0x125, 0x3, 0x2, 0x2, 0x2, 0x127, 0x126, 0x3, 0x2, 0x2, 0x2, 
    0x128, 0x2f, 0x3, 0x2, 0x2, 0x2, 0x129, 0x12a, 0x7, 0xf, 0x2, 0x2, 0x12a, 
    0x31, 0x3, 0x2, 0x2, 0x2, 0x12b, 0x12c, 0x7, 0x10, 0x2, 0x2, 0x12c, 
    0x33, 0x3, 0x2, 0x2, 0x2, 0x12d, 0x12e, 0x7, 0x11, 0x2, 0x2, 0x12e, 
    0x35, 0x3, 0x2, 0x2, 0x2, 0x12f, 0x130, 0x9, 0x4, 0x2, 0x2, 0x130, 0x37, 
    0x3, 0x2, 0x2, 0x2, 0x131, 0x137, 0x5, 0x3a, 0x1e, 0x2, 0x132, 0x137, 
    0x5, 0x3c, 0x1f, 0x2, 0x133, 0x137, 0x5, 0x3e, 0x20, 0x2, 0x134, 0x137, 
    0x5, 0x40, 0x21, 0x2, 0x135, 0x137, 0x5, 0x42, 0x22, 0x2, 0x136, 0x131, 
    0x3, 0x2, 0x2, 0x2, 0x136, 0x132, 0x3, 0x2, 0x2, 0x2, 0x136, 0x133, 
    0x3, 0x2, 0x2, 0x2, 0x136, 0x134, 0x3, 0x2, 0x2, 0x2, 0x136, 0x135, 
    0x3, 0x2, 0x2, 0x2, 0x137, 0x39, 0x3, 0x2, 0x2, 0x2, 0x138, 0x139, 0x9, 
    0x5, 0x2, 0x2, 0x139, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x13a, 0x13e, 0x7, 0x1a, 
    0x2, 0x2, 0x13b, 0x13e, 0x7, 0x1b, 0x2, 0x2, 0x13c, 0x13e, 0x5, 0x34, 
    0x1b, 0x2, 0x13d, 0x13a, 0x3, 0x2, 0x2, 0x2, 0x13d, 0x13b, 0x3, 0x2, 
    0x2, 0x2, 0x13d, 0x13c, 0x3, 0x2, 0x2, 0x2, 0x13e, 0x3d, 0x3, 0x2, 0x2, 
    0x2, 0x13f, 0x140, 0x9, 0x6, 0x2, 0x2, 0x140, 0x3f, 0x3, 0x2, 0x2, 0x2, 
    0x141, 0x142, 0x9, 0x7, 0x2, 0x2, 0x142, 0x41, 0x3, 0x2, 0x2, 0x2, 0x143, 
    0x144, 0x9, 0x8, 0x2, 0x2, 0x144, 0x43, 0x3, 0x2, 0x2, 0x2, 0x20, 0x47, 
    0x50, 0x61, 0x6a, 0x74, 0x7d, 0x81, 0x8b, 0x93, 0x99, 0x9f, 0xa5, 0xa9, 
    0xaf, 0xb1, 0xb4, 0xbd, 0xc2, 0xe0, 0xe4, 0xf3, 0xf9, 0x102, 0x107, 
    0x10d, 0x117, 0x120, 0x127, 0x136, 0x13d, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

olgaParser::Initializer olgaParser::_init;
