
// Generated from olga.g4 by ANTLR 4.8

#pragma once


#include "antlr4-runtime.h"
#include "olgaParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by olgaParser.
 */
class  olgaVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by olgaParser.
   */
    virtual antlrcpp::Any visitCompilationUnit(olgaParser::CompilationUnitContext *context) = 0;

    virtual antlrcpp::Any visitExternalDeclaration(olgaParser::ExternalDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitCompoundStatement(olgaParser::CompoundStatementContext *context) = 0;

    virtual antlrcpp::Any visitJumpStatement(olgaParser::JumpStatementContext *context) = 0;

    virtual antlrcpp::Any visitIfStatement(olgaParser::IfStatementContext *context) = 0;

    virtual antlrcpp::Any visitWhileStatement(olgaParser::WhileStatementContext *context) = 0;

    virtual antlrcpp::Any visitFunctionCall(olgaParser::FunctionCallContext *context) = 0;

    virtual antlrcpp::Any visitExpression(olgaParser::ExpressionContext *context) = 0;

    virtual antlrcpp::Any visitAssignmentExpression(olgaParser::AssignmentExpressionContext *context) = 0;

    virtual antlrcpp::Any visitDeclaration(olgaParser::DeclarationContext *context) = 0;

    virtual antlrcpp::Any visitDefinition(olgaParser::DefinitionContext *context) = 0;

    virtual antlrcpp::Any visitSizeSpecifier(olgaParser::SizeSpecifierContext *context) = 0;

    virtual antlrcpp::Any visitPointerDeclaration(olgaParser::PointerDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitVariableDeclaration(olgaParser::VariableDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitArrayDeclaration(olgaParser::ArrayDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitPointerDefinition(olgaParser::PointerDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitVariableDefinition(olgaParser::VariableDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitArrayDefinition(olgaParser::ArrayDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitInitializer(olgaParser::InitializerContext *context) = 0;

    virtual antlrcpp::Any visitFunctionDefinition(olgaParser::FunctionDefinitionContext *context) = 0;

    virtual antlrcpp::Any visitParameterDeclarationList(olgaParser::ParameterDeclarationListContext *context) = 0;

    virtual antlrcpp::Any visitParameterDeclaration(olgaParser::ParameterDeclarationContext *context) = 0;

    virtual antlrcpp::Any visitPrefixOperator(olgaParser::PrefixOperatorContext *context) = 0;

    virtual antlrcpp::Any visitDereferenceOperator(olgaParser::DereferenceOperatorContext *context) = 0;

    virtual antlrcpp::Any visitReferenceOperator(olgaParser::ReferenceOperatorContext *context) = 0;

    virtual antlrcpp::Any visitNegationOperator(olgaParser::NegationOperatorContext *context) = 0;

    virtual antlrcpp::Any visitAssignmentOperator(olgaParser::AssignmentOperatorContext *context) = 0;

    virtual antlrcpp::Any visitR_operator(olgaParser::R_operatorContext *context) = 0;

    virtual antlrcpp::Any visitArithmeticalOperator(olgaParser::ArithmeticalOperatorContext *context) = 0;

    virtual antlrcpp::Any visitLogicalOperator(olgaParser::LogicalOperatorContext *context) = 0;

    virtual antlrcpp::Any visitRelationalOperator(olgaParser::RelationalOperatorContext *context) = 0;

    virtual antlrcpp::Any visitShiftOperator(olgaParser::ShiftOperatorContext *context) = 0;

    virtual antlrcpp::Any visitBitOperator(olgaParser::BitOperatorContext *context) = 0;


};

