grammar olga;

compilationUnit : externalDeclaration+;

//GLOBAL SPACE DECLARATIONS
externalDeclaration
    : functionDefinition
    | declaration ';'
    | definition ';'
    ;
////////////

//STATEMENTS
compoundStatement
    : jumpStatement
    | declaration ';'
    | definition ';'
    | assignmentExpression ';'
    | ifStatement
    | whileStatement
    | functionCall ';'
    ;

jumpStatement
    : Return (Literal | Identifier) ';'
    | Break ';'
    | Continue ';'
    ;

ifStatement : If '(' expression ')' '{' compoundStatement* '}' ('else' '{' compoundStatement* '}')?;

whileStatement : While '(' expression ')' '{' compoundStatement* '}';

functionCall: Identifier '(' expression? (',' expression)* ')';
/////////////////////////

//EXPRESSIONS
expression
    :
    (prefixOperator)? (Identifier | Literal | functionCall | initializer)
    (r_operator (prefixOperator)? (Identifier | Literal | functionCall | initializer))?
    ;

assignmentExpression: (prefixOperator)? Identifier assignmentOperator expression;
/////////////////////////

// DECLARATIONS / DEFINITIONS
//variables
declaration: pointerDeclaration | variableDeclaration | arrayDeclaration;

definition: pointerDefinition | variableDefinition | arrayDefinition;

sizeSpecifier
    : U8 | U16 | U32
    | I8 | I16 | I32
    ;

pointerDeclaration: Ptr Identifier ':' '<' sizeSpecifier '>';

variableDeclaration: Let Identifier ':' '<' sizeSpecifier '>'; //TODO: check during compilation for positive ints only

arrayDeclaration: Let Identifier ':' '<' sizeSpecifier ',' Literal '>';

pointerDefinition: pointerDeclaration '=' ((prefixOperator? Identifier) | Literal);

variableDefinition: variableDeclaration '=' expression;

arrayDefinition: arrayDeclaration '=' initializer;

initializer: '<' sizeSpecifier '>' '{' expression? (',' expression)* '}';
//
//functions
functionDefinition
    :  Fn Identifier '(' parameterDeclarationList? ')' ('->' sizeSpecifier)?
    '{' compoundStatement* '}'
    ;

parameterDeclarationList: parameterDeclaration (',' parameterDeclaration)*;

parameterDeclaration: Identifier ':' '<' sizeSpecifier (',' Literal)? '>'; //TODO: size or pointer?
//
/////////////////////////

// OPERATORS
prefixOperator
    : negationOperator
    | dereferenceOperator
    | referenceOperator
    ;

dereferenceOperator: '*';

referenceOperator: '&';

negationOperator: '!';


assignmentOperator: '=' | '+=' | '-=' | '*=' | '/=';

//the name "operator" conflicts with Cpp, hence the prefix "r_" is used
r_operator: arithmeticalOperator | logicalOperator | relationalOperator | shiftOperator | bitOperator;

arithmeticalOperator: '+' | '-' | '*' | '/' | '%';

logicalOperator: '||' | '&&' | negationOperator;

relationalOperator: '<' | '>' | '<=' | '>=' | '!='| '==';

shiftOperator: '<<' | '>>';

bitOperator: '|' | '^' | '&';
/////////////////////



//LEXER
//keywords
If: 'if';
While: 'while';
Let: 'let';
Ptr: 'ptr';
Break: 'break';
Continue: 'continue';
Return: 'return';
Fn: 'fn';
//type specifiers
U8: 'u8';
I8: 'i8';

U16: 'u16';
I16: 'i16';

U32: 'u32';
I32: 'i32';
/////////////////////

Identifier : Character(Character | Number)*;

Literal
    : '"' (EscapeSequence | ~["\\])* '"'
    | '\'' (EscapeSequence | ~["\\])? '\''
    | 'True'
    | 'False'
    | NumericLiteral
    | NumericLiteral '.' Number+
    ;

NumericLiteral: '-'? Number+;

StringLiteral : '"' (EscapeSequence | ~["\\])* '"';

fragment
Character: [a-zA-Z_] | '_';

fragment
Number: [0-9];

fragment EscapeSequence : '\\' . ;

//other
WS : [ \t\r\n]+ -> skip ;

LINE_COMMENT : '//' ~[\r\n]* -> skip ;