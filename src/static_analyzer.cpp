//
// Created by Artur Twardzik on 16/12/2023.
//

#include "static_analyzer.h"

#include "../tests/doctest.h"

std::map<std::string, std::vector<std::string>> create_calls_map(
        const std::vector<Fn *> &functions,
        const GlobalSymbolTable &global_symbol_table) {
    std::map<std::string, std::vector<std::string>> calls;

    for (const auto &function: functions) {
        std::vector<std::vector<std::string>> calls_within;

        if (!global_symbol_table.is_declared_globally(function->identifier)) {
            fprintf(stderr, "\r[!] Could not find function: `%s`. Aborting.\n", function->identifier.c_str());
            exit(1);
        }

        calls.insert(std::make_pair(function->identifier, function->calls_within));
    }

    return calls;
}

void print_calls(const std::map<std::string, std::vector<std::string>> &calls) {
    std::cout << "\n-------------- Function Calls --------------\n";

    for (const auto &fn: calls) {
        std::cout << "@" << fn.first << ": ";
        std::string fn_calls{};

        for (const auto &call: fn.second) {
            fn_calls += call + ", ";
        }
        fn_calls = "[" + fn_calls.substr(0, fn_calls.length() - 2) + "]";

        std::cout << fn_calls << std::endl;
    }

    std::cout << "--------------------------------------------\n";
}

std::vector<FnCall *> create_call_graph(std::map<std::string, std::vector<std::string>> &calls) {
    std::vector<FnCall *> call_graph;

    for (const auto &function: calls) {
        FnCall *outer_call = new FnCall(function.first);

        if (!function.second.empty()) {
            outer_call->contains_calls = true;

            for (const auto &called: function.second) {
                FnCall *call = new FnCall(called);

                if (!calls[called].empty()) {
                    call->contains_calls = true;
                }

                outer_call->inner_calls.push_back(call);
            }
        }

        call_graph.push_back(outer_call);
    }

    return call_graph;
}

std::string get_following_calls(
        const FnCall *searched_call,
        const FnCall *current_call,
        size_t &current_depth,
        const std::vector<FnCall *> &calls) {
    if (!current_call->contains_calls) {
        return "";
    }

    if (MAXIMAL_RECURSIVE_DEPTH == current_depth) {
        return " . . . ";
    }

    if (current_depth && current_call->identifier == searched_call->identifier) {
        return current_call->identifier;
    }
    else {
        current_depth += 1;

        std::string following_calls{};
        std::string inner_calls_names{};
        for (const auto *inner_call: current_call->inner_calls) {
            inner_calls_names += inner_call->identifier + ", ";
            const FnCall *next_call = *std::find_if(calls.begin(), calls.end(), [inner_call](const FnCall *fn_call) {
                return fn_call->identifier == inner_call->identifier;
            }); //we know it is not nullptr

            following_calls += get_following_calls(searched_call, next_call, current_depth, calls);

            if (!following_calls.empty()) {
                break;
            }
        }

        if (following_calls.empty()) {
            following_calls = "(" + inner_calls_names.substr(0, inner_calls_names.length() - 2) + ")";
        }

        return current_call->identifier + " -> " + following_calls;
    }
}

bool has_recursive_call_chain(std::string_view call_chain) {
    std::vector<std::string> tokens = tokenize(call_chain);

    for (const auto &token: tokens) {
        if (std::count(tokens.begin(), tokens.end(), token) > 1) {
            return true;
        }
    }

    return false;
}

std::vector<std::string> tokenize(std::string_view str) {
    std::vector<std::string> tokens;

    std::string temp{};
    for (const char &letter: str) {
        if ('(' == letter) break;
        if (isalnum(letter)) {
            temp += letter;
        }
        else {
            if (!temp.empty()) {
                tokens.push_back(temp);
                temp = "";
            }
        }
    }

    tokens.push_back(temp);

    return tokens;
}