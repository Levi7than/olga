//
// Created by Artur Twardzik on 16/12/2023.
//

#ifndef OLGA_STATIC_ANALYZER_H
#define OLGA_STATIC_ANALYZER_H

#include <iostream>
#include <map>
#include <utility>
#include <vector>
#include <string_view>
#include <string>

#include "ast.h"
#include "symbol_table.h"

#define MAXIMAL_RECURSIVE_DEPTH 10

struct FnCall {
    std::string identifier;

    bool contains_calls = false;
    std::vector<FnCall *> inner_calls;

    FnCall() = delete;

    explicit FnCall(std::string identifier) : identifier(std::move(identifier)) {}
};

/**
 * Transforms provided vector of functions into map of functions and their inner calls.
 * Panics if any function is not declared.
 * */
std::map<std::string, std::vector<std::string>> create_calls_map(
        const std::vector<Fn *> &functions,
        const GlobalSymbolTable &global_symbol_table);

/**
 * Prints functions and their corresponding inner calls.
 * */
void print_calls(const std::map<std::string, std::vector<std::string>> &calls);

/**
 * Transforms functions and their inner calls into the vector of FnCall.
 * Only one layer is transformed. I.e. Function and it's first children.
 * */
std::vector<FnCall *> create_call_graph(std::map<std::string, std::vector<std::string>> &calls);

/**
 * Traverses provided function and searches for specified function repetition (recursive call chain).
 * Please note that this function may be extremely inefficient for larger codebases, as it's complexity oscillates around exponential.
 * Therefore maximum traversal depth is set by MAXIMAL_RECURSIVE_DEPTH (default set to 10).
 *
 * \Usage
 * Please note that this function searches ONLY for specified function. Therefore following situation may happen for \b foo analysis: \n
 * - foo -> factorial -> factorial -> factorial -> factorial ->  . . .\n\n
 * However for \b factorial analysis the output will be:\n
 * - factorial -> factorial\n
 * */
std::string get_following_calls(
        const FnCall *searched_call,
        const FnCall *current_call,
        size_t &current_depth,
        const std::vector<FnCall *> &calls);

/**
 * Checks for repeated function calls
 * */
bool has_recursive_call_chain(std::string_view call_chain);

/**
 * Transforms the form of function call chain into tokens. E.g. \n\n
 * - foo -> bar -> foo \n\n
 * - foo bar foo
 * */
static std::vector<std::string> tokenize(std::string_view str);

#endif //OLGA_STATIC_ANALYZER_H
