//
// Created by Artur Twardzik on 18/11/2023.
//

#include "symbol_table.h"

#include "../tests/doctest.h"

void GlobalSymbolTable::insert_globally(SymbolEntry symbol_entry) {
    if (is_declared_globally(symbol_entry.identifier)) {
        std::cerr << "[!] Line: " << symbol_entry.line_number << ". Symbol `" << symbol_entry.identifier
                  << "` already declared.\n";
        exit(1);
    }

    if (symbol_entry.owns_scope()) {
        scope_symbol_tables.push_back(ScopeSymbolTable{symbol_entry.identifier});
    }

    global_symbols.push_back(symbol_entry);
}

void GlobalSymbolTable::insert_in_scope(SymbolEntry symbol_entry, std::string_view scope_name) {
    if (auto scope = std::ranges::find_if(scope_symbol_tables, [=](const ScopeSymbolTable &scope_symbol_table) {
            return scope_symbol_table.scope_name == scope_name;
        }); scope != scope_symbol_tables.end()) {
        scope->insert(symbol_entry);
    }
    else {
        std::cerr << "[!] Line: " << symbol_entry.line_number << ". Cannot insert in scope `" << scope_name
                  << "`. Scope not found!\n";
        exit(1);
    }
}

const SymbolEntry *GlobalSymbolTable::lookup(std::string_view identifier, std::string_view scope_name) const {
    SymbolEntry symbol_entry;

    if (scope_name.empty()) {
        return lookup_global_scope(identifier);
    }

    return lookup_local_scope(identifier, scope_name);
}

const SymbolEntry *GlobalSymbolTable::lookup_global_scope(std::string_view identifier) const {
    if (auto result = std::ranges::find_if(global_symbols,
                                           [=](const SymbolEntry &symbol_entry) {
                                               return symbol_entry.identifier == identifier;
                                           });
            result != global_symbols.end()) {
        return &*result;
    }

    return nullptr;
}

const SymbolEntry *
GlobalSymbolTable::lookup_local_scope(std::string_view identifier, std::string_view scope_name) const {
    if (auto local_scope = std::ranges::find_if(scope_symbol_tables,
                                                [=](const ScopeSymbolTable &symbol_table) {
                                                    return symbol_table.scope_name == scope_name;
                                                });
            local_scope != scope_symbol_tables.end()) {
        return local_scope->lookup(identifier);
    }

    return lookup_global_scope(identifier);
}

std::string GlobalSymbolTable::get_global_printable() const {
    // Fuck you C++ and your streams. The std::format is still not implemented properly in C++23! So let me use my C strings.
    //
    // This function is intentionally written in C standard with the extension of std::string in order not to concern about
    // cleanup in other parts of the compiler.
    //
    // Also, according to C coding standards, in order to avoid code repetition and cleanup allocated memory, goto statements
    // are used.
    //
    // https://www.kernel.org/doc/html/v4.19/process/coding-style.html#:~:text=The%20goto%20statement%20comes%20in,or%20why%20the%20goto%20exists.
    // "The goto statement comes in handy when a function exits from multiple locations and some common work such as cleanup
    // has to be done. If there is no cleanup needed then just return directly." ~Linux Kernel Coding Style
    //
    // FIXME: as soon as the std::format is implemented replace C-style code with C++ counterpart.

    std::string buffer;
    CFormat format = get_symbol_table_format();

    char *buf = (char *) malloc(sizeof(char) * format.allocation_length);
    if (buf == NULL) {
        fprintf(stderr, "[!] Could not allocate memory for global symbol table as string. Skipping.\n");
        goto end;
    }

    if (uint16_t bytes_written = sprintf(
                buf, format.format.c_str(), "SCOPE", "LINE", "SYMBOL", "SYMBOL TYPE", "DATA SIZE"); bytes_written > 0) {
        buffer += buf;
        buffer += std::string(bytes_written - 1, '-') + '\n';
    }
    else {
        fprintf(stderr, "[!] Unexpected error occurred while writing to string buffer. Skipping.\n");
        goto end;
    }

    for (const auto &symbol_entry: global_symbols) {
        memset(buf, 0, strlen(buf));

        const char *data_type = type_to_string(symbol_entry.symbol_type);
        const char *data_size = size_to_string(symbol_entry.data_size);

        if (sprintf(buf, format.format.c_str(),
                    "GLOBAL",
                    std::to_string(symbol_entry.line_number).c_str(),
                    symbol_entry.identifier.c_str(),
                    data_type,
                    data_size) < 0) {
            fprintf(stderr, "[!] Unexpected error occurred while writing to string buffer.\n");
            goto end;
        }
        buffer += buf;

        if (symbol_entry.owns_scope()) {
            buffer += get_local_printable(symbol_entry.identifier, format);
        }
    }

    end:
    free(buf);
    return buffer;
}

const CFormat GlobalSymbolTable::get_symbol_table_format() const {
    const uint16_t max_global_name_length = find_longest_global_name();
    const uint16_t max_local_name_length = find_longest_local_name();
    const uint16_t scope_length = max_global_name_length > 20 ? max_global_name_length : 20;
    const uint16_t symbol_length = max_local_name_length > 20 ? max_local_name_length : 20;

    const uint16_t allocation_length = scope_length + symbol_length + 100;

    const std::string format = "| %-" + std::to_string(scope_length) + "s:"
                               + " %-" + std::to_string(max_line_number_length) + "s |"
                               + " %-" + std::to_string(symbol_length) + "s |"
                               + " %-" + std::to_string(max_symbol_type_length) + "s |"
                               + " %-" + std::to_string(max_data_size_length) + "s |\n";

    return CFormat{format, allocation_length};
}

uint16_t GlobalSymbolTable::find_longest_global_name() const {
    uint16_t longest{};

    for (const auto &symbol: global_symbols) {
        if (symbol.owns_scope()) {
            change_if_greater(longest, symbol.identifier.length());
        }
    }

    return longest;
}

uint16_t GlobalSymbolTable::find_longest_local_name() const {
    uint16_t longest{};

    for (const auto &symbol: global_symbols) {
        change_if_greater(longest, symbol.identifier.length());
    }

    for (const auto &scope: scope_symbol_tables) {
        change_if_greater(longest, scope.find_longest_local_symbol_name());
    }

    return longest;
}

std::string GlobalSymbolTable::get_local_printable(std::string_view identifier, const CFormat &format) const {
    std::string buffer;

    auto result = std::ranges::find_if(scope_symbol_tables,
                                       [=](const ScopeSymbolTable &symbol_entry) {
                                           return symbol_entry.scope_name == identifier;
                                       });

    if (result != scope_symbol_tables.end()) {
        buffer += result->get_local_printable(format);
    }

    return buffer;
}

bool GlobalSymbolTable::is_declared_globally(std::string_view identifier) const {
    if (lookup_global_scope(identifier)) {
        return true;
    }

    return false;
}

void ScopeSymbolTable::insert(SymbolEntry symbol_entry) {
    if (is_declared_in_scope(symbol_entry.identifier)) {
        std::cerr << "[!] Line: " << symbol_entry.line_number << ". Symbol `" << symbol_entry.identifier
                  << "` already declared.\n";
        exit(1);
    }

    scope_symbols.push_back(symbol_entry);
}

const SymbolEntry *ScopeSymbolTable::lookup(std::string_view identifier) const {
    if (auto result = std::ranges::find_if(scope_symbols, [=](const SymbolEntry &symbol_entry) {
            return symbol_entry.identifier == identifier;
        }); result != scope_symbols.end()) {
        return &*result;
    }

    return nullptr;
}

std::string ScopeSymbolTable::get_local_printable(const CFormat &format) const {
    // For further reference why C style code is used go to GlobalSymbolTable::get_global_printable() definition.
    //FIXME: as soon as the std::format is implemented replace C-style code with C++ counterpart.
    std::string buffer{};

    if (format.format.empty()) {
        fprintf(stderr, "[!] Format not specified. Skipping.\n");
        return buffer;
    }

    char *buf = (char *) malloc(sizeof(char) * format.allocation_length);
    if (buf == NULL) {
        fprintf(stderr, "[!] Could not allocate memory for global symbol table as string\n");
        goto end;
    }

    for (const auto &symbol_entry: scope_symbols) {
        memset(buf, 0, strlen(buf));

        const char *data_type = type_to_string(symbol_entry.symbol_type);
        const char *data_size = size_to_string(symbol_entry.data_size);

        if (sprintf(buf, format.format.c_str(),
                    scope_name.c_str(),
                    std::to_string(symbol_entry.line_number).c_str(),
                    symbol_entry.identifier.c_str(),
                    data_type,
                    data_size) < 0) {
            fprintf(stderr, "[!] Unexpected error occurred while writing to string buffer.\n");
            goto end;
        }
        buffer += buf;
    }

    end:
    free(buf);
    return buffer;
}

uint16_t ScopeSymbolTable::find_longest_local_symbol_name() const {
    uint16_t longest{};

    for (const auto &symbol: scope_symbols) {
        change_if_greater(longest, symbol.identifier.length());
    }

    return longest;
}

bool ScopeSymbolTable::is_declared_in_scope(std::string_view identifier) const {
    if (lookup(identifier)) {
        return true;
    }

    return false;
}

TEST_SUITE_BEGIN("Symbol Table");

TEST_CASE("test_global_insert") {
    GlobalSymbolTable global_symtab;
    SymbolEntry entry = {
            .identifier = "foo",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };

    global_symtab.insert_globally(entry);

    SymbolEntry inserted_entry = global_symtab.global_symbols.back();

    CHECK_EQ(inserted_entry, entry);
}

TEST_CASE("test_scope_insert") {
    ScopeSymbolTable scope_symtab{.scope_name = "foo"};
    SymbolEntry scope_entry = {
            .identifier = "bar",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };

    scope_symtab.insert(scope_entry);

    CHECK_EQ(scope_entry, scope_symtab.scope_symbols.back());
}

TEST_CASE("test_function_scope_creation") {
    GlobalSymbolTable global_symtab;

    SymbolEntry function = {
            .identifier = "foo",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Function,
            .line_number = 0,
    };
    global_symtab.insert_globally(function);

    ScopeSymbolTable inserted_function_scope = global_symtab.scope_symbol_tables.back();
    SymbolEntry inserted_function = global_symtab.global_symbols.back();

    CHECK_EQ(inserted_function_scope.scope_name, function.identifier);
    CHECK_EQ(inserted_function, function);
}

TEST_CASE("test_filling_function_scope") {
    GlobalSymbolTable global_symtab;

    SymbolEntry function = {
            .identifier = "foo",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Function,
            .line_number = 0,
    };
    SymbolEntry local_variable = {
            .identifier = "bar",
            .data_size = DataSize::U32,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };
    global_symtab.insert_globally(function);
    global_symtab.insert_in_scope(local_variable, function.identifier);

    const ScopeSymbolTable &local_variable_scope = global_symtab.scope_symbol_tables.back();
    SymbolEntry local_variable_entry = local_variable_scope.scope_symbols.back();

    CHECK_EQ(local_variable, local_variable_entry);
}

TEST_CASE("test_global_symbol_table_lookup") {
    GlobalSymbolTable global_symtab;

    SymbolEntry symbol1 = {
            .identifier = "foo",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Function,
            .line_number = 0,
    };
    SymbolEntry symbol2 = {
            .identifier = "bar",
            .data_size = DataSize::U32,
            .symbol_type = SymbolType::Function,
            .line_number = 0,
    };
    global_symtab.insert_globally(symbol1);
    global_symtab.insert_globally(symbol2);

    SymbolEntry returned_symbol = *global_symtab.lookup("foo");

    CHECK_EQ(returned_symbol, symbol1);
}

TEST_CASE("test_scope_symbol_table_lookup") {
    GlobalSymbolTable global_symtab;

    SymbolEntry function = {
            .identifier = "foo",
            .data_size = DataSize::Void,
            .symbol_type = SymbolType::Function,
            .line_number = 0,
    };
    SymbolEntry local_variable = {
            .identifier = "bar",
            .data_size = DataSize::U32,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };
    global_symtab.insert_globally(function);
    global_symtab.insert_in_scope(local_variable, function.identifier);

    SymbolEntry returned_symbol = *global_symtab.lookup("bar", "foo");
}

TEST_CASE("test_getting_global_printable") {
    GlobalSymbolTable global_symtab;
    SymbolEntry entry = {
            .identifier = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu tortor eu odio pretium mollis biam.",
            .data_size = DataSize::U32,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };
    global_symtab.insert_globally(entry);

    std::string expected_result = "| SCOPE               : LINE   | SYMBOL                                                                                               | SYMBOL TYPE  | DATA SIZE  |\n"
                                  "-------------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
                                  "| GLOBAL              : 0      | Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis eu tortor eu odio pretium mollis biam. | Variable     | u32        |\n";

    CHECK_EQ(global_symtab.get_global_printable(), expected_result);
}

TEST_CASE("test_getting_local_printable") {
    ScopeSymbolTable scope_symbol_table{.scope_name = "Foo"};
    SymbolEntry entry = {
            .identifier = "Lorem ipsum dolor sit amet, consectetur tincidunt.",
            .data_size = DataSize::U32,
            .symbol_type = SymbolType::Variable,
            .line_number = 0,
    };
    scope_symbol_table.scope_symbols.push_back(entry);

    CFormat format{
            .format = "| %s : %s | %s | %s | %s |",
            .allocation_length = 150,
    };

    std::string expected_result = "| Foo : 0 | Lorem ipsum dolor sit amet, consectetur tincidunt. | Variable | u32 |";

    CHECK_EQ(scope_symbol_table.get_local_printable(format), expected_result);
}

TEST_SUITE_END();
