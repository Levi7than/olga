//
// Created by Artur Twardzik on 18/11/2023.
//

#ifndef OLGA_SYMBOL_TABLE_H
#define OLGA_SYMBOL_TABLE_H

#include <iostream>
#include <iomanip>
#include <format>
#include <vector>
#include <string>
#include <sstream>
#include <string_view>
#include <algorithm>

const uint16_t max_line_number_length = 6;
const uint16_t max_symbol_type_length = 12;
const uint16_t max_data_size_length = 10;

/**
 * \brief Struct holding C-style formatting
 */
struct CFormat {
    std::string format{};
    uint16_t allocation_length{};
};

enum DataSize {
    Void, Undefined,
    U8, U16, U32,
    I8, I16, I32,
};

/**
 * \return Returns width of requested data size
 */
inline uint16_t get_data_width(const DataSize &data_size) {
    switch (data_size) {
        case Void:
            return 1;
        case U8:
        case I8:
            return 1;
        case U16:
        case I16:
            return 2;
        case U32:
        case I32:
            return 4;
        case Undefined:
            return 0;
    }
}

/**
 * \brief Converts requested data size to corresponding enum.
 * \param data_size String holding data size.
 * \return Enum element.
 */
inline DataSize determine_data_size(std::string_view data_size) {
    if (data_size == "void") return DataSize::Void;
    else if (data_size == "u8") return DataSize::U8;
    else if (data_size == "u16") return DataSize::U16;
    else if (data_size == "u32") return DataSize::U32;
    else if (data_size == "i8") return DataSize::I8;
    else if (data_size == "i16") return DataSize::I16;
    else if (data_size == "i32") return DataSize::I32;
    else return DataSize::Undefined;
}

/**
 * \brief Converts requested data size to corresponding name.
 * \param data_size Enum element holding data size.
 * \return Data size name
 */
inline const char *size_to_string(const DataSize &data_size) {
    switch (data_size) {
        case Void:
            return "void";
        case U8:
            return "u8";
        case U16:
            return "u16";
        case U32:
            return "u32";
        case I8:
            return "i8";
        case I16:
            return "i16";
        case I32:
            return "i32";
        case Undefined:
            return "Undefined";
    }
}

enum SymbolType {
    Variable, Function,
};

inline const char *type_to_string(SymbolType symbol_type) {
    switch (symbol_type) {
        case Variable:
            return "Variable";
        case Function:
            return "Function";
    }
}

enum ScopeType {
    Global, Local
};

struct SymbolEntry {
    std::string identifier{};
    DataSize data_size;
    SymbolType symbol_type;
    uint32_t address{};
    size_t line_number{};

    [[nodiscard]]
    inline bool owns_scope() const {
        if (symbol_type == SymbolType::Function) {
            return true;
        }

        return false;
    }

    bool operator==(const SymbolEntry &other) const = default;
};

struct ScopeSymbolTable {
    std::string scope_name{};
    std::vector<SymbolEntry> scope_symbols;

    /**
     * Inserts symbol (declaration) into the local scope.
     * */
    void insert(SymbolEntry symbol_entry);

    /**
     * Checks if the identifier exists in scope.
     * @param scope_name Scope to be searched in. The default is global scope.
     * */
    const SymbolEntry *lookup(std::string_view identifier) const;

    /**
     * Provides pretty version of local symbol table
     * */
    std::string get_local_printable(const CFormat &format) const;

    /**
     * Checks if the identifier already exists in local scope
     * */
    bool is_declared_in_scope(std::string_view identifier) const;

    /**
     * Iterates through defined symbols to find the longest one.
     * */
    uint16_t find_longest_local_symbol_name() const;
};

struct GlobalSymbolTable {
    std::vector<SymbolEntry> global_symbols;
    std::vector<ScopeSymbolTable> scope_symbol_tables;

    /**
     * Inserts symbol (declaration) into the global scope.
     * */
    void insert_globally(SymbolEntry symbol_entry);

    /**
     * Inserts symbol (declaration) into specified scope (declarations that own scope e.g. functions).
     * */
    void insert_in_scope(SymbolEntry symbol_entry, std::string_view scope_name);

    /**
     * Checks if the identifier exists in scope.
     * @param scope_name Scope to be searched in. The default is global scope.
     * */
    const SymbolEntry *lookup(std::string_view identifier, std::string_view scope_name = "") const;

    /**
     * Provides pretty version of global symbol table
     * */
    std::string get_global_printable() const;

    /**
     * Provides symbol table format for C-style strings.
     * */
    const CFormat get_symbol_table_format() const;

    /**
     * Provides pretty version of specified local symbol table
     * */
    std::string get_local_printable(std::string_view identifier, const CFormat &format) const;

    /**
     * Checks if the identifier already exists in global scope
     * */
    bool is_declared_globally(std::string_view identifier) const;

    /**
     * Searches for the identifier in the global scope
     * @returns Pointer to the SymbolEntry with given identifier.
     * */
    const SymbolEntry *lookup_global_scope(std::string_view identifier) const;

    /**
     * Searches for the identifier in the specified local scope.
     * Please note that unless local scope contains the symbol, the global scope will be looked up
     * @returns Pointer to the SymbolEntry with given identifier.
     * */
    const SymbolEntry *lookup_local_scope(std::string_view identifier, std::string_view scope_name) const;

    /**
     * Iterates through defined global symbols to find the longest one.
     * */
    uint16_t find_longest_global_name() const;

    /**
     * Iterates through defined local scope symbols to find the longest one.
     * */
    uint16_t find_longest_local_name() const;
};

/**
 * Changes left parameter to the right if L < R
 * */
static inline void change_if_greater(uint16_t &left, uint16_t right) {
    if (right > left) left = right;
}

#endif //OLGA_SYMBOL_TABLE_H
